close all
clear all

c=linspecer(9);

%% data load
fil1 = load('ISSTF01.asc', '-ascii');
f1 = fil1(:,1);
m1 = fil1(:,4);
p1 = fil1(:,5);


fil2 = load('ISSTF02.asc', '-ascii');
f2 = fil2(:,1);
m2 = fil2(:,4);
p2 = fil2(:,5);


fil3 = load('ISSTF03.asc', '-ascii');
f3 = fil3(:,1);
m3 = fil3(:,4);
p3 = fil3(:,5);

UGF = f1(find(m1<0,1,'first'));
UGP = p1(find(m1<0,1,'first'));
PM = 180+UGP;
figure()
subplot(211)
semilogx(f1,m1,'Color',c(3,:))
hold on
semilogx(f2,m2,'Color',c(1,:))
semilogx(UGF,0,'mo')
% semilogx(f3,m3)
grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('Before ISS Board Work','After ISS Board Work',strcat(strcat('UGP=',num2str(round(UGF*1e-3,1))),'kHz'))
title('ISS Loop TF')
xlim([1e3,max(f2)])
ylim([-20,60])

subplot(212)
semilogx(UGF,UGP,'mo')
hold on
semilogx(f1,p1,'Color',c(3,:))
semilogx(f2,p2,'Color',c(1,:))

% semilogx(f3,p3)
grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend(strcat(strcat('PM= ',num2str(round(PM,1))),' deg'))
xlim([1e3,max(f2)])
ylim([-180,180])
