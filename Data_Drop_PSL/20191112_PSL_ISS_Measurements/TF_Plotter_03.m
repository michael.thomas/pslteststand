% close all
% clear all

c=linspecer(9);

%% data load
fil1 = load('INT1OFF.asc', '-ascii');
f1 = fil1(:,1);
m1 = fil1(:,4);
p1 = fil1(:,5);


fil2 = load('INT2OFF.asc', '-ascii');
f2 = fil2(:,1);
m2 = fil2(:,4);
p2 = fil2(:,5);


fil3 = load('ISSIO01.asc', '-ascii');
f3 = fil3(:,1);
m3 = fil3(:,4);
p3 = fil3(:,5);

%% Modelling the ISS Board Filters
[a1,b1,c1,d1] = transint(870,4680,0.4624);
[a2,b2,c2,d2] = transint(100,4230,-0.3432);
[a11,b11,c11,d11] = integrator(4680,10^(-21.3/20));
[a22,b22,c22,d22] = integrator(4230,-10^(-41.8/20));

sys1 = ss(a1,b1,c1,d1);
sys2 = ss(a2,b2,c2,d2);
sys  = series(sys1,-sys2);
sys11 = ss(a11,b11,c11,d11);
sys22 = ss(a22,b22,c22,d22);
sysA  = series(sys11,-sys22);

[gfit1,pfit1] = bode(f1,sys1);
gfit1=20.*log10(gfit1(:));
pfit1=(pfit1(:));
[gfit2,pfit2] = bode(f1,sys2);
gfit2=20.*log10(gfit2(:));
pfit2=(pfit2(:));
[gfit,pfit] = bode(f1,sys);
gfit=20.*log10(gfit(:).*9);
pfit=(pfit(:));

[gint1,pint1] = bode(f1,sys11);
gint1=20.*log10(gint1(:));
pint1=(pint1(:));
[gint2,pint2] = bode(f1,sys22);
gint2=20.*log10(gint2(:));
pint2=(pint2(:));
[gint,pint] = bode(f1,sysA);
gint=20.*log10(gint(:).*9);
pint=(pint(:));


LoogG = 133+20.*log10(10.^(gint./20)./f1);

%% Plotting results
figure()
subplot(211)
semilogx(f1,m1,'Color',c(3,:))
hold on
semilogx(f2,m2,'Color',c(1,:))
semilogx(f3,m3,'Color',c(2,:))
semilogx(f1,gfit1,'--','Color',c(3,:))
semilogx(f1,gfit2,'--','Color',c(1,:))
semilogx(f1,gfit,'--','Color',c(2,:))
semilogx(f1,gint1,':','Color',c(3,:))
semilogx(f1,gint2,':','Color',c(1,:))
semilogx(f1,gint+45,':','Color',c(2,:))
semilogx(f1,LoogG,':','Color',c(4,:))

grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
title('ISS Board TF with Integrators OFF')
legend('Integrator 1','Integrator 2','ISS Servo Box (out/in)','TransInt(870Hz,4680Hz)','TransInt(100Hz,4230Hz)','Overall')
xlim([min(f1),max(f2)])
ylim([-50,5])

subplot(212)
semilogx(f1,p1,'Color',c(3,:))
hold on
semilogx(f2,p2,'Color',c(1,:))
semilogx(f3,p3,'Color',c(2,:))
semilogx(f1,pfit1,'--','Color',c(3,:))
semilogx(f1,pfit2,'--','Color',c(1,:))
semilogx(f1,pfit,'--','Color',c(2,:))
semilogx(f1,pint1,':','Color',c(3,:))
semilogx(f1,pint2,':','Color',c(1,:))
semilogx(f1,pint,':','Color',c(2,:))

grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
xlim([min(f1),max(f2)])
ylim([-180,180])
