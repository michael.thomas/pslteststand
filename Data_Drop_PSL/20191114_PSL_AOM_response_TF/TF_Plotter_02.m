close all
clear all
%%
fil1 = load('AOM2PDAC.asc', '-ascii'); 
f1 = fil1(:,1);
m1 = fil1(:,4);
p1 = fil1(:,5);

fil2 = load('AOM2PDDC.asc', '-ascii'); 
f2 = fil2(:,1);
m2 = fil2(:,4);
p2 = fil2(:,5);

%%
[a,b,c,d] = transint(800,4.3e3,1);
sys=ss(a,b,c,d);
[gfil,pfil] = bode(sys,f1);
[a,b,c,d] = integrator(4.3e3,0.08);
sys=ss(a,b,c,d);
[gfil2,pfil2] = bode(sys,f1);
%% Plot
figure()
subplot(211)
semilogx(f1,m1,'r','LineWidth',3)
hold on
semilogx(f2,m2,'b','LineWidth',3)
% semilogx(f1,20.*log10(gfil(:)),'k--')
% semilogx(f1,20.*log10(gfil2(:)),'k:')

grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('AOM Out -> PDA AC','AOM Out -> PDA DC')
xlim([min(f1),max(f1)])
ylim([0,50])

subplot(212)
semilogx(f1,p1,'r','LineWidth',3)
hold on
semilogx(f2,p2,'b','LineWidth',3)
% semilogx(f1,pfil(:),'k--')
% semilogx(f1,pfil2(:),'k:')

grid on
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
ylim([-180,180])
xlim([min(f1),max(f1)])
