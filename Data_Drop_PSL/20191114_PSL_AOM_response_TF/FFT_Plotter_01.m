% close all
% clear all
c=linspecer(9);

%% data load: Noise FFT with inner ISS loop on only A is in-loop B is out of loop
fil1 = load('1LFFTA1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('1LFFTA2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('1LFFTA3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('1LFFTA4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F1A=[f4;f3;f2;f1];
M1A=[m4;m3;m2;m1];
N1A=(M1A)./1.95;

fil1 = load('1LFFTB1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('1LFFTB2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('1LFFTB3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('1LFFTB4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F1B=[f4;f3;f2;f1];
M1B=[m4;m3;m2;m1];
N1B=(M1B)./1.95;

%% data load: Noise FFT with both inner and outer ISS loops on A is in-loop B is out of loop

fil1 = load('2LFFTA1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('2LFFTA2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('2LFFTA3.asc', '-ascii'); 
f3 = fil3(22:801,1);
m3 = fil3(22:801,4);
F2A=[f3;f2;f1];
M2A=[m3;m2;m1];
N2A=(M2A)./1.95;

fil1 = load('2LFFTB1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('2LFFTB2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('2LFFTB3.asc', '-ascii'); 
f3 = fil3(22:801,1);
m3 = fil3(22:801,4);

F2B=[f3;f2;f1];
M2B=[m3;m2;m1];
N2B=(M2B)./1.95;
%% data load: Noise FFT of unstabilised noise, both loops off
fil1 = load('UNFFTB1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('UNFFTB2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('UNFFTB3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('UNFFTB4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F3=[f4;f3;f2;f1];
M3=[m4;m3;m2;m1];
N3=(M3./1);

%% Loading ISS_PD_FILT TF to convert from voltage noise at measurement point (TRANSFER1B on ISS board) to laser noise
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD = 10.^(LISOPD.data(:,2)./20);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(gPD,pPD);
PDTF = interp1(fPD,PDTF0,F1A);
PDTF2 = interp1(fPD,PDTF0,F2B);
%Converting error point noise to laser intensity noise
n1A = abs(N1A./PDTF);
n1B = abs(N1B./PDTF);
n2A = abs(N2A./PDTF2);
n2B = abs(N2B./PDTF2);
n3 = abs(N3./PDTF);

%% plotting results
figure()
loglog(F1A,n1A./10,'Color',c(1,:))
hold on
loglog(F1B,n1B./10,':','Color',c(1,:))
loglog(F2A,n2A./10,'Color',c(3,:))
loglog(F2B,n2B./10,':','Color',c(3,:))

loglog(F3,n3./10,'Color',c(2,:))

grid on
legend('ISS first loop (in loop)','ISS first loop (out of loop)','ISS both loops (in-loop)','ISS both loops (out-of-loop)','Unstabilised Noise')
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
xlim([0,max(F1A)])
% %ylim([-75,-30])

% Estimating Loop Gain from noise stabilisation
N2a = interp1(F2A,N2A,F3);
N2b = interp1(F2B,N2B,F3);

G1 = 20.*log10(N3./N1A);
G2 = 20.*log10(N3./N1B);
G3 = 20.*log10(N3./N2a);
G4 = 20.*log10(N3./N2b);

figure()
semilogx(F3,G1,'Color',c(1,:))
hold on
semilogx(F3,G2,':','Color',c(1,:))
semilogx(F3,G3,'Color',c(3,:))
semilogx(F3,G4,':','Color',c(3,:))


grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'Gain (dB)'},'Interpreter','latex')
legend('ISS first loop (in loop)','ISS first loop (out of loop)','ISS both loops (in-loop)','ISS both loops (out-of-loop)')
xlim([0,max(F3)])