clear
% load noise of amp i 
FRN = srs780DataStitcher({load('FRN1.asc'),load('FRN2.asc'),...
    load('FRN3.asc'),load('FRN4.asc')});

%amp i
low_f_amp_i = importdata('AMPI04.txt');
med_f_amp_i = importdata('AMPI05.txt');
high_f_amp_i = importdata('AMPI07.txt');

low_f_amp_i_data = low_f_amp_i.data;
med_f_amp_i_data = med_f_amp_i.data;
high_f_amp_i_data = high_f_amp_i.data;


% figure(1)
% semilogx(low_f_amp_i_data(:,1),low_f_amp_i_data(:,2),...
%     med_f_amp_i_data(:,1),med_f_amp_i_data(:,2),...
%     high_f_amp_i_data(:,1),high_f_amp_i_data(:,2))
% xlabel('Frequency (Hz)')
% ylabel('dBm/\surd(Hz)')
% title('Amp I')
% axis tight
% print('Amp I','-dpng')

%% amp ii
low_f_amp_ii = importdata('AMPII04.txt');
med_f_amp_ii = importdata('AMPII07.txt');
med_f_amp_ii_2 = importdata('AMPII05.txt');
high_f_amp_ii = importdata('AMPII06.txt');

low_f_amp_ii_data = low_f_amp_ii.data;
med_f_amp_ii_data = med_f_amp_ii.data;
high_f_amp_ii_data = high_f_amp_ii.data;
med_f_amp_ii_2_data = med_f_amp_ii_2.data;

% figure(2)
% semilogx(low_f_amp_ii_data(:,1),low_f_amp_ii_data(:,2),...
%     med_f_amp_ii_data(:,1),med_f_amp_ii_data(:,2),...
%     high_f_amp_ii_data(:,1),high_f_amp_ii_data(:,2),...
%     med_f_amp_ii_2_data(:,1),med_f_amp_ii_2_data(:,2))
% xlabel('Frequency (Hz)')
% ylabel('dBm/\surd(Hz)')
% title('Amp II')
% axis tight
% print('Amp II','-dpng')
%%

data = [low_f_amp_ii,med_f_amp_ii,med_f_amp_ii_2,high_f_amp_ii];
all_spectrum = [];
for(i = data)
    RBW = 1;
    RBW_string = strsplit(i.textdata{11});
    spectrum = i.data;
    %disp(RBW_string{2})
    if(strcmp(RBW_string{3},'kHz'))
        RBW = str2num(RBW_string{2}).*1e3;
    elseif(strcmp(RBW_string{3},'Hz'))
        RBW = str2num(RBW_string{2});
    else
        RBW = str2num(RBW_string{2}).*1e6;
    end
    % get in dbm
    spectrum_dbm = spectrum(:,2) + 10.*log10(RBW);
    % convert to volts by knowing that the impedance is 100 ohm
    all_spectrum = [all_spectrum,sqrt(100.*0.001.*10.^(spectrum_dbm./10)/RBW)];
    
    
end
%divide by 9.3V to get RIN
all_spectrum = all_spectrum./(sqrt(2).*9.3)

figure(3)
loglog(low_f_amp_ii_data(:,1),all_spectrum(:,1),...
    med_f_amp_ii_data(:,1),all_spectrum(:,2),...
    med_f_amp_ii_2_data(:,1),all_spectrum(:,3),...
    high_f_amp_ii_data(:,1),all_spectrum(:,4))
axis tight
ylabel('RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
title('Amp II')



%%

data = [low_f_amp_i,med_f_amp_i,high_f_amp_i];
all_spectrum = [];
for(i = data)
    RBW = 1;
    RBW_string = strsplit(i.textdata{11});
    spectrum = i.data;
    %disp(RBW_string{2})
    if(strcmp(RBW_string{3},'kHz'))
        RBW = str2num(RBW_string{2}).*1e3;
    elseif(strcmp(RBW_string{3},'Hz'))
        RBW = str2num(RBW_string{2});
    else
        RBW = str2num(RBW_string{2}).*1e6;
    end
    % get in dbm
    spectrum_dbm = spectrum(:,2) + 10.*log10(RBW);
    % convert to volts by knowing that the impedance is 100 ohm
    all_spectrum = [all_spectrum,sqrt(100.*0.001.*10.^(spectrum_dbm./10)/RBW)];
    
    
end
%divide by 9.3V to get RIN
all_spectrum = all_spectrum./(sqrt(2).*9.6)

figure(4)
loglog(FRN(:,1),FRN(:,2)./10.8,...
    low_f_amp_i_data(:,1),all_spectrum(:,1),...
    med_f_amp_i_data(:,1),all_spectrum(:,2),...
    high_f_amp_i_data(:,1),all_spectrum(:,3))
axis tight
ylabel('RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
title('Amp I')
 