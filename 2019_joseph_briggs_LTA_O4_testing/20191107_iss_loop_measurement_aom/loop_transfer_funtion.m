%% load data
clear

load ISSM101.asc -ASCII;
load ISSM201.asc -ASCII;

%% calculate Loop TF

f = ISSM101(:,1);

G = ISSM201(:,2)+1i*ISSM201(:,3);
open_loop = 1./(ISSM101(:,2)+1i*ISSM101(:,3));

open_loop_mag = 20.*log10(abs(open_loop));
open_loop_phase = 180./pi.*angle(open_loop);
%% plot results

figure(1)
subplot(2,1,1)
semilogx(ISSM101(:,1),ISSM101(:,4))
xlabel('Frequency (Hz)')
ylabel('dB')
axis tight
subplot(2,1,2)
semilogx(ISSM101(:,1),ISSM101(:,5))
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
axis tight


figure(3)
subplot(2,1,1)
semilogx(f,open_loop_mag)
xlabel('Frequency (Hz)')
ylabel('dB')
axis tight
subplot(2,1,2)
semilogx(f,open_loop_phase)
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
axis tight


figure(2)
subplot(2,1,1)
semilogx(ISSM201(:,1),ISSM201(:,4))
xlabel('Frequency (Hz)')
ylabel('dB')
axis tight
subplot(2,1,2)
semilogx(ISSM201(:,1),ISSM201(:,5))
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
axis tight