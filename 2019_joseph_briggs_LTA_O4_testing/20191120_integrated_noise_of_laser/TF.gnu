reset
set logscale x
set xrange[0.00616595:162181]
db2abs(x)=10.**(x/20.)
w(x)=180/pi*arg(x)
ww(x)=w(x)
wwp(x)=(w(x)>=0)?w(x):w(x)+360
wwm(x)=(w(x)<=0)?w(x):w(x)-360
abs2db(x)=20.*log10(abs(x))
i={0,1}
set xlabel "Frequency [Hz]"
set ylabel "dB "
set y2label "Phase [Degree]"
set ytics nomirror
set y2tics nomirror
set y2tics -180,45,180
set my2tics 3
set y2range [-180:180]
set mxtics 10
set mytics 10
z(x,y)=x+i*y
set key outside
set nolog y
set output "NUL"
set term windows
set size ratio .5
set key below
set grid xtics ytics
plot\
"TF.out" using ($1):($2) axes x1y1 title "U(n8) [V/V] dB " w l lt 1 lw 2,\
"TF.out" using ($1):($3) axes x1y2 title "U(n8) [V/V] Phase " w l lt 2 lw 2
pause -1 "Hit return to continue"
