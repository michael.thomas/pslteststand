clear
load laser_noise

liso = importdata('TF_AC_PD.OUT');
PD_TF_LISO = liso.data;
%F1 = frequency (Hz)
%N1 = RIN/sqrt(Hz) 
%M1 = V/sqrt(Hz)

DC_light_level = 10; %V

%need to flip it to make it go backwards in frequency, then flip the result
%so you can plot it forwards
RIN_rms = sqrt(fliplr(cumulative_integration(fliplr(F1),fliplr(N1.*N1))));

N = 2;
colours = linspecer(N);

figure(1)
loglog(F1,N1,'Color',colours(1,:))
hold on
loglog(F1,RIN_rms,'Color',colours(2,:))
hold off
axis([2 1.023e5 1e-7 1e-1])
ylabel('RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
legend('Free Running Laser noise','Integrated laser noise')

figure(2)
loglog(F1,N1.*DC_light_level,'Color',colours(1,:))
hold on
loglog(F1,RIN_rms.*10,'Color',colours(2,:))
hold off
axis([2 1.023e5 1e-6 1e0])
ylabel('V_{rms}/\surd(Hz)')
xlabel('Frequency (Hz)')
legend('Free Running Laser noise (DC = 10V)','Integrated laser noise (DC = 10V)')


%%

[a,b,c,d] = transdif(0.07,3.3,sqrt(0.2));
td1 = ss(a,b,c,d);
[a,b,c,d] = transdif(0.07,3.1,sqrt(0.2));
td2 = ss(a,b,c,d);
[a,b,c,d] = transint(130,1100,1);
ti = ss(a,b,c,d);
[a,b,c,d] = lowpass(1000,-1);
lp = ss(a,b,c,d);
ac_pd = series(ti,series(lp,series(td1,td2)));

[pd_ac_mag,pd_ac_phase] = bode(ac_pd,PD_TF_LISO(:,1).*2.*pi);

pd_ac_mag = squeeze(pd_ac_mag);
pd_ac_phase = squeeze(pd_ac_phase);

figure(4)
subplot(2,1,1)
semilogx(PD_TF_LISO(:,1),PD_TF_LISO(:,2))
hold on
semilogx(PD_TF_LISO(:,1),20.*log10(pd_ac_mag))
hold off
subplot(2,1,2)
semilogx(PD_TF_LISO(:,1),PD_TF_LISO(:,3))
hold on
semilogx(PD_TF_LISO(:,1),(pd_ac_phase))
hold off
%%

[pd_ac_mag,pd_ac_phase] = bode(ac_pd,F1.*2.*pi);

pd_ac_mag = squeeze(pd_ac_mag);

RIN_rms_ac = sqrt(fliplr(cumulative_integration(fliplr(F1),fliplr(N1.*N1.*pd_ac_mag.*pd_ac_mag))));


figure(3)
loglog(F1,N1.*DC_light_level.*pd_ac_mag,'Color',colours(1,:))
hold on
loglog(F1,RIN_rms_ac.*DC_light_level,'Color',colours(2,:))
hold off
axis tight
ylabel('V_{rms}/\surd(Hz)')
xlabel('Frequency (Hz)')
legend('Free Running Laser noise, AC ouput (DC = 10V)','Integrated laser noise AC ouput (DC = 10V)')