clear
load laser_noise


iss_requirement_noise = iss_requirement_line(F1');
loop_requirement = N1./iss_requirement_noise';
N = 2;
colours = linspecer(N);

figure(1)
loglog(F1,N1,'Color',colours(1,:))
hold on
loglog(F1,iss_requirement_noise,'Color',colours(2,:))
hold off
axis tight
ylabel('RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
legend('Free Running Laser noise','Noise requirement')
%%
f = logspace(-1,6,1000);
[a,b,c,d] = transdif(0.001,1,1);
td1 = ss(a,b,c,d);
[a,b,c,d] = transint(4,1000,1);
ti = ss(a,b,c,d);
[a,b,c,d] = lowpass(4,10);
lp = ss(a,b,c,d);
[a,b,c,d] = transdif(5e3,1e5,1);
td2 = ss(a,b,c,d);

open_loop_servo = series(td1,series(td1,series(lp,series(ti,td2))));
closed_loop_servo = feedback(open_loop_servo,1);

[mag_model_OL, phase_model_OL] = bode(open_loop_servo,f.*2.*pi);
[mag_model_CL, phase_model_CL] = bode(closed_loop_servo,f.*2.*pi);


mag_model_OL = squeeze(mag_model_OL);
phase_model_OL = squeeze(phase_model_OL);

mag_model_CL = squeeze(mag_model_CL);
phase_model_CL = squeeze(phase_model_CL);

figure(2)
subplot(2,1,1)
semilogx(F1,20.*log10(loop_requirement))
hold on
semilogx(f,20.*log10(mag_model_OL),'k')
hold off
legend('Requirement','Proposed servo shape')
axis tight

subplot(2,1,2)
semilogx(f,phase_model_OL,'k')
axis tight



