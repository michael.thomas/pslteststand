power = [61.5,61.3,61.0,60.8,60.3,59.0];
flow_rate =[2,1.75,1.5,1.25,1,0.7];

figure(1)
scatter(flow_rate,power,'filled')
xlabel('Flow rate (litres/min)')
ylabel('Power (W)')
title('Amp II power as flow rate was changed')

print('power_flow_rate','-dpng')