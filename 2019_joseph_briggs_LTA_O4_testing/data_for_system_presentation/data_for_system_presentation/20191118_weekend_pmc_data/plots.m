%% load in data X2:PSL-DBB_MODE_NUM
clear

%choose gps seconds OSC_BASEPL_CENTTEMPfor end time, converter here https://www.andrews.edu/~tzs/timeconv/timeconvert.php
end_time = 1258123955;
duration = 2*60*60*24;
start_time = end_time - duration;

chans = {'X2:PSL-PWR_PMC_TRANS_OUTPUT',...
    'X2:PSL-PWR_PMC_REFL_OUTPUT',...
    'X2:PSL-PWR_PMC_SUM',...
    'X2:PSL-PMC_TRIGGER_OUTPUT',...
    'X2:PSL-PMC_HEATER_CALI_INMON'...
    'X2:PSL-PMC_TEMP_OUTPUT',...
    'X2:PSL-PMC_HV_MON_AVG',...
    'X2:PSL-PMC_GAIN',...
    'X2:PSL-OSC_BASEPL_FRONTLTEMP',...
    'X2:PSL-OSC_BASEPL_CENTTEMP',...
    'X2:PSL-OSC_BOXHUM',...
    'X2:PSL-ISS_AOM_DRIVER_MON_OUTPUT',...
    'X2:PSL-OSC_BASEPL_CENTTEMP'};
%min_trend_data =  get_data(chans,'minute',start_time,duration);
%save('pmc_weekend','min_trend_data')
load pmc_weekend
%sec_trend_data =  get_data(chans,'second',start_time,duration);

%% make variables time series  

time = 0:1:length(min_trend_data(1).data)-1; %time series in steps of the data rate (1 sample/min)
time_hour = time./60; %hours
PMC_trans = min_trend_data(1).data; %Watts
PMC_ref = min_trend_data(2).data; %Watts
PMC_sum = min_trend_data(3).data; %Watts
PMC_trigger = min_trend_data(4).data; %Volts
PMC_heater = min_trend_data(5).data; %Volts?
PMC_temp = min_trend_data(6).data; %Kelvin?
PMC_HV_mon = min_trend_data(7).data; %Volts (1:50 of actual voltage)
PMC_gain = min_trend_data(8).data; %gain of PMC loop, no units
PMC_temp_control_signal = min_trend_data(9).data; %not sure on units
TNT_temp = min_trend_data(9).data; %deg C
TNT_humidity = min_trend_data(11).data; %water/m^3?
AOM = min_trend_data(12).data;
amp_i_temperature = min_trend_data(13).data;
%% Plot time series
N = 2;
c = linspecer(1);
fs = 1/60
lp_f = 0.001;

figure(6)
subplot(4,1,1)
rms_error_bar_fill(PMC_trans,time_hour,lp_f,fs,c)
xlabel('Time (hour)')
ylabel('PMC transmitted Power (Watt)')
hold off
grid minor
axis tight
set(gca, 'FontSize', 12)

subplot(4,1,2)
rms_error_bar_fill(PMC_sum,time_hour,lp_f,fs,c)
xlabel('Time (hour)')
ylabel('PMC Sum Power (Watt)')
hold off
grid minor
axis tight
set(gca, 'FontSize', 12)
%print('npro_overnight_power','-dpng')

subplot(4,1,3)
rms_error_bar_fill(PMC_temp,time_hour,lp_f,fs,c)
xlabel('Time (hour)')
ylabel('PMC temperature (�C)')
hold off
grid minor
axis tight
set(gca, 'FontSize', 12)
%print('temperature_overnight','-dpng')

subplot(4,1,4)
rms_error_bar_fill(AOM,time_hour,0.001,fs,c)
xlabel('Time (hour)')
ylabel('AOM signal')
hold off
grid minor
axis tight
set(gca, 'FontSize', 12)
print('PMC_weekend','-dpng','-r0')

%%

figure(2)
plot(time_hour,PMC_trans./(PMC_sum).*100)
xlabel('Time (h)')
ylabel('Transmitted light/Total light (%)')
axis tight
grid minor
print('transmission_percentage','-dpng')
disp('Mean PMC transmission/PMC sum:')
disp([num2str(mean(PMC_trans./(PMC_sum).*100)),'+-',num2str(std(PMC_trans./(PMC_sum).*100))])

%%
figure(3)
subplot(2,1,1)
rms_error_bar_fill(PMC_sum,time_hour,0.001,fs,c)
axis tight
grid minor
ylabel('PMC sum power (Watt)')
xlabel('time (Hour)')
set(gca, 'FontSize', 12)

subplot(2,1,2)
%rms_error_bar_fill(amp_i_temperature,time_hour,0.001,fs,c)
plot(time_hour,amp_i_temperature,'Color',c)
xlabel('time (Hour)')
axis tight
ylabel('Second Amplifier Temperature (�C)')
set(gca, 'FontSize', 12)

grid minor
print('amp_i_temperature','-dpng','-r0')
%%
figure(1)
subplot(3,3,1)
plot(time_hour,PMC_trans)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC transmitted power')
axis tight
grid minor

subplot(3,3,2)
plot(time_hour,PMC_ref)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC reflected power')
axis tight
grid minor

subplot(3,3,3)
plot(time_hour,PMC_sum)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC sum power')
axis tight
grid minor

subplot(3,3,4)
plot(time_hour,PMC_trigger)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('PMC trigger signal')
axis tight
grid minor

subplot(3,3,7)
plot(time_hour,TNT_temp)
xlabel('Time (hour)')
ylabel('Temperature (°C)')
title('Lab Temperature')
axis tight
grid minor

subplot(3,3,8)
plot(time_hour,PMC_temp)
xlabel('Time (hour)')
ylabel('Temperature (K)')
title('PMC Temperature')
axis tight
grid minor

subplot(3,3,6)
plot(time_hour,PMC_heater)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('PMC heater signal')
axis tight
grid minor

subplot(3,3,5)
plot(time_hour,PMC_HV_mon)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('PMC HV mon (1:50)')
axis tight
grid minor

subplot(3,3,9)
plot(time_hour,TNT_humidity)
xlabel('Time (hour)')
ylabel('Humidity (relative)')
title('TNT lab Humidity')
axis tight
grid minor

set(gcf,'Position',[0 0 900 900])

annotation('textbox', [0 0.9 1 0.1], ...
    'String', ['gps start time = ',num2str(1258123955)], ...
    'EdgeColor', 'none', ...
    'HorizontalAlignment', 'center')

figure_1_name = ['PMC_state_',num2str(start_time),'to_',num2str(end_time)];
print(figure_1_name,'-dpng')
disp('Mean PMC transmission:')
disp([num2str(mean(PMC_trans)),'+-',num2str(std(PMC_trans))])