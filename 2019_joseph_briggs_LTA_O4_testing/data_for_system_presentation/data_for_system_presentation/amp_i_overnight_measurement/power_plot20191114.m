%% load in data 1257783832
clear

%choose gps seconds for end time, converter here:
% https://www.andrunits?ews.edu/~tzs/timeconv/timeconvert.php
end_time = 1257783832;
duration = 60*60*12;
start_time = end_time - duration;

% chans = {'X2:PSL-OSC_PWR2',...
%     'X2:PSL-OSC_BOXHUM',...
%     'X2:PSL-OSC_BOXTEMP',...
%     'X2:PSL-PWR_NPRO_OUTPUT'};
% min_trend_data =  get_data(chans,'minute',start_time,duration);
% sec_trend_data =  get_data(chans,'second',start_time,duration);

%save('amp_i_overnight','min_trend_data','sec_trend_data')
load amp_i_overnight

time_sec = 0:1:length(sec_trend_data(1).data)-1;
time_min = 0:1:length(min_trend_data(1).data)-1; %time series in steps of the data rate (1 sample/min)
time_hour = time_min./60; %hours

amp_i_power = min_trend_data(1).data; %Watts
humidity = min_trend_data(2).data; %relative humidity
temperature = min_trend_data(3).data; %deg c
npro = min_trend_data(4).data; % Watts

%% 
N = 1;
c = linspecer(N);
fs = 1/60;
lp_f = 0.001;
figure(6)
subplot(4,1,1)
rms_error_bar_fill(amp_i_power,time_hour,lp_f,fs,c)
ylabel({'Second Amplifier','Power (W)'})
hold off
grid on
set(gca,'xticklabel',[])
set(gca,'YLim',[138.5 139.5]);
set(gca, 'FontSize', 19)

subplot(4,1,2)
rms_error_bar_fill(npro,time_hour,lp_f,fs,c)
ylabel({'NPRO','Power (W)'})
set(gca,'xticklabel',[])
set(gca,'YLim',[1.609 1.61]);
hold off
grid on
set(gca, 'FontSize', 19)
%print('npro_overnight_power','-dpng')

subplot(4,1,3)
rms_error_bar_fill(temperature,time_hour,lp_f,fs,c)
ylabel({'TnT lab','temperature (�C)'})
set(gca,'xticklabel',[])
hold off
grid on
axis tight
set(gca, 'FontSize', 19)
%print('temperature_overnight','-dpng')

subplot(4,1,4)
rms_error_bar_fill(humidity,time_hour,0.001,fs,c)
xlabel('Time (hour)')
ylabel({'TnT lab','humidity (%)'})
hold off
grid on
axis tight
set(gca, 'FontSize', 19)
print('amp_i_overnight','-dpng','-r0')

%%
%set up measurement
fs = 1; %sample rate is 1 per sec i.e 1Hz
f_nyquist = fs/2;
%choose this number (Hz)
f_resolution = 5e-4
len = floor(fs/f_resolution);
f_resolution = fs/len;
L = length(time_sec)

%make round to number of the form 2^a 3^b 5^c 7^d 11^e 13^f
% Make the ASD by following this guide: https://holometer.fnal.gov/GH_FFT.pdf

j = linspace(1,len,len);
z = 2*pi.*j./len;
window_fn = 0.5.*(1-cos(z)); %Hann window

%calculation of effective noise bandwidth, sometimes this is useful
s1 = sum(window_fn);
s2 = sum(window_fn.*window_fn);
ENBW = fs*s2/(s1^2);
NENBW = len*s2/(s1^2);
overlap = 0.5;
averages = ceil((L/len)/(1-overlap));
disp('...')
disp(['nyquist frequency: ',num2str(f_nyquist),' Hz'])
disp(['frequency Resolution: ',num2str(f_resolution),' Hz'])
disp(['Number of frequency Bins: ',num2str(len), ' of width ',...
num2str(f_resolution), ' Hz'])
disp(['Check for speediness of FFTW: ',num2str(factor(len))])
disp(['Effective Noise Bandwidth: ',num2str(ENBW), 'Hz'])
disp(['Window overlap: ',num2str(100*overlap), '%']);
disp(['Normalised Effective Noise Bandwidth: ',num2str(NENBW), ' bins']);
disp(['Number of averages = ',num2str(averages)])
%% pwelch calculation

amp_i_power_s = sec_trend_data(1).data; %Watts
humidity_s = sec_trend_data(2).data; %relative humidity
temperature_s = sec_trend_data(3).data; %deg c
npro_s = sec_trend_data(4).data; % Watts

amp_i_power_DC_remove = amp_i_power_s-mean(amp_i_power_s);
humidity_DC_remove = humidity_s - mean(humidity_s);
temperature_DC_remove = temperature_s - mean(temperature_s);
npro_DC_remove = npro_s-mean(npro_s);


[amp_i_power_PSD,f] = pwelch(amp_i_power_DC_remove,window_fn,floor(overlap*len),len,fs);
humidity_PSD = pwelch(humidity_DC_remove,window_fn,floor(overlap*len),len,fs);
temperature_PSD = pwelch(temperature_DC_remove,window_fn,floor(overlap*len),len,fs);
npro_PSD = pwelch(npro_DC_remove,window_fn,floor(overlap*len),len,fs);


power_humidity_coherence = mscohere(amp_i_power_DC_remove,humidity_DC_remove,window_fn,floor(overlap*len),len,fs);
power_temperature_coherence = mscohere(amp_i_power_DC_remove,temperature_DC_remove,window_fn,floor(overlap*len),len,fs);
power_npro_coherence = mscohere(amp_i_power_DC_remove,npro_DC_remove,window_fn,floor(overlap*len),len,fs);

amp_i_power_ASD = sqrt(amp_i_power_PSD);
humidity_ASD = sqrt(humidity_PSD);
temperature_ASD = sqrt(temperature_PSD);
npro_ASD = sqrt(npro_PSD);
%% 
N = 1;
c = linspecer(N);
figure(2)
subplot(4,1,1)
loglog(f,amp_i_power_ASD./mean(amp_i_power_s),'Color',c)
ylabel('Second amplifier RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
%title('Amp ii power')
grid on
axis tight
set(gca, 'FontSize', 12)

subplot(4,1,2)
loglog(f,npro_ASD/mean(npro_s),'Color',c)
ylabel('NPRO RIN/\surd(Hz)')
xlabel('Frequency (Hz)')
%title('NPRO power')
grid on
axis tight
set(gca, 'FontSize', 12)

subplot(4,1,3)
loglog(f,temperature_ASD,'Color',c)
ylabel('TnT lab temperature�C/\surd(Hz)')
xlabel('Frequency (Hz)')
%title('Temperature')
grid on
axis tight
set(gca, 'FontSize', 12)

subplot(4,1,4)
loglog(f,humidity_ASD,'Color',c)
xlabel('Frequency (Hz)')
ylabel('relative humidity/\surd(Hz)')
%title('Humidity')
grid on
axis tight
set(gca, 'FontSize', 12)
print('asd_amp_i_low_f','-dpng','-r0')
%%
N = 3;
c = linspecer(N);
figure(3)
axes('NextPlot','replacechildren', 'ColorOrder',c);
semilogx(f,power_humidity_coherence,f,power_temperature_coherence,f,power_npro_coherence)
xlabel('Frequecny (Hz)')
ylabel('Coherence')
lgd = legend('Second amplifier power and lab humididty','Second amplifier power and lab temperature','Second amplifier and NPRO power','location','northeast');
set(lgd,'FontSize',12)
grid on
axis tight
set(gca, 'FontSize', 12)

set(gca, 'FontSize', 12)