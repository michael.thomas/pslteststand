#Measured frome surface of m38(clamps on table show where ruler was).
#ruler position (cm) mean diameter (um) waist (um e^2)
22.1   3220    1610
11.7   2865    1432.5
14.5   2955    1477.5
24.9   3330    1665
19.0   3105    1552.5
16.9   3060    1530
30.0   3500    1750

#To get further, moved the ruler back 16.8cm
#13.2   3500     1750
#15.4   3550     1775
#22.1   3840     1920
