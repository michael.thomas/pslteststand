clear

%noise spectrums, probably all shite
%laser_noise = srs780DataStitcher({load('DEC05-01.asc'),load('DEC05-02.asc'),...
%    load('DEC05-03.asc'),load('DEC05-04.asc')});

%PDA_stabilised = srs780DataStitcher({load('DEC05-31.asc'),load('DEC05-33.asc'),...
%    load('DEC05-35.asc'),load('DEC05-37.asc')});

%PDB_stabilised = srs780DataStitcher({load('DEC05-32.asc'),load('DEC05-34.asc'),...
%    load('DEC05-36.asc'),load('DEC05-38.asc')});

%PD_DC_dark_noise = srs780DataStitcher({load('DEC05-21.asc'),load('DEC05-23.asc'),...
%    load('DEC05-25.asc'),load('DEC05-27.asc')});
%PD_AC_dark_noise = srs780DataStitcher({load('DEC05-22.asc'),load('DEC05-24.asc'),...
%    load('DEC05-26.asc'),load('DEC05-28.asc')});

%error_point = srs780DataStitcher({load('DEC05-09.asc'),load('DEC05-10.asc'),...
%    load('DEC05-11.asc'),load('DEC05-12.asc')});

%IL = srs780DataStitcher({load('DEC05-39.asc'),load('DEC05-41.asc'),...
%    load('DEC05-43.asc'),load('DEC05-45.asc')});

%OOL = srs780DataStitcher({load('DEC05-40.asc'),load('DEC05-42.asc'),...
%    load('DEC05-44.asc'),load('DEC05-46.asc')});

%PDA_2 = srs780DataStitcher({load('DEC05-48.asc'),load('DEC05-50.asc'),...
%    load('DEC05-52.asc'),load('DEC05-54.asc')});

%TFA = srs780DataStitcher({load('DEC05-47.asc'),load('DEC05-49.asc'),...
%    load('DEC05-51.asc'),load('DEC05-53.asc')});

%transfer functions
open_loop_tf = load('DEC05-17.asc');
open_loop_tf_zoom = load('DEC05-20.asc');
board_no_int = load('DEC05-29.asc');
AOM = load('DEC05-30.asc');
AOM_with_linearing = load('DEC06-01.asc');

%finally fucking fiugred out my noise problem
IL = srs780DataStitcher({load('IL01.asc'),load('IL02.asc'),...
    load('IL03.asc'),load('IL04.asc')});
OOL = srs780DataStitcher({load('OOL01.asc'),load('OOL02.asc'),...
    load('OOL03.asc'),load('OOL04.asc')});
FRN = srs780DataStitcher({load('FRN1.asc'),load('FRN2.asc'),...
    load('FRN3.asc'),load('FRN4.asc')});
DARK = srs780DataStitcher({load('DARK01.asc'),load('DARK02.asc'),...
    load('DARK03.asc'),load('DARK04.asc')});
ANA = srs780DataStitcher({load('ANA01.asc'),load('ANA02.asc'),...
    load('ANA03.asc'),load('ANA04.asc')});
%%
% PDA_DC = 11.17;
% PDB_DC = 11.37;
IL_DC = 9.83;
OOL_DC = 10.03;
FRN_DC = 10.8;
DC_2 = 9.54;

shot_noise = sqrt(2*1.6e-19*3.3e3./OOL_DC).*ones(length(FRN(:,1)),1);

noise = sqrt(2*shot_noise.^2+(IL(:,2)./IL_DC).^2 + (ANA(:,2)./OOL_DC).^2 + 2.*(DARK(:,2)./OOL_DC).^2);

%%
figsize = [25 25 900 900];

C = linspecer(6);

h1 = figure(1);
clf
axes('NextPlot','replacechildren','ColorOrder',C)
loglog(FRN(:,1),FRN(:,2)./FRN_DC,...
    IL(:,1),IL(:,2)./IL_DC,...
    OOL(:,1),OOL(:,2)./OOL_DC,...
    DARK(:,1),DARK(:,2)./OOL_DC,...
    ANA(:,1),ANA(:,2)./OOL_DC,...
    FRN(:,1),noise)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('Free running noise','IL photodiode','OOL photodiode','PD Dark noise','Analyser noise','Noise Budget')
axis tight
set(gca,'FontSize',12)
print('AOM_stabilisation_noise','-dpng')
print('AOM_stabilisation_noise','-depsc')

%set(h1,'Position',figsize)

%%
C = linspecer(5);
h2 = figure(2);
clf
subplot(2,1,1)
semilogx(open_loop_tf(:,1),open_loop_tf(:,4),'Color',C(1,:))
hold on
semilogx(open_loop_tf_zoom(:,1),open_loop_tf_zoom(:,4),'Color',C(2,:))
semilogx(AOM(:,1),AOM(:,4),'Color',C(3,:))
semilogx(board_no_int(10:end,1),board_no_int(10:end,4),'Color',C(4,:))
semilogx(AOM_with_linearing(10:end,1),AOM_with_linearing(10:end,4),'Color',C(5,:))
hold off
axis tight
legend('Open Loop with AOM and ISS (gain = 22)','OLTF zoom in (gain = 22)',...
    'AOM to PD DC output','D1001985 Input to J8 (gain = 22)','x60 to PD DC output')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
set(gca,'FontSize',12)

subplot(2,1,2)
semilogx(open_loop_tf(:,1),open_loop_tf(:,5),'Color',C(1,:))
hold on
semilogx(open_loop_tf_zoom(:,1),open_loop_tf_zoom(:,5),'Color',C(2,:))
semilogx(AOM(:,1),AOM(:,5),'Color',C(3,:))
semilogx(board_no_int(10:end,1),board_no_int(10:end,5),'Color',C(4,:))

semilogx(AOM_with_linearing(200:end,1),unwrap(AOM_with_linearing(200:end,5),180),'Color',C(5,:))
hold off
axis tight
xlabel('Frequency (Hz)')
ylabel('Phase (degree)')
set(gca,'FontSize',12)
print('AOM_stabilisation_TF','-depsc')
print('AOM_stabilisation_TF','-dpng')

%set(h2,'Position',figsize)