close all
clear all
c=linspecer(9);
%% Watchdog after amp II data load

fil5 = load('WD1FFT05.asc', '-ascii'); 
f5 = fil5(102:801,1);
m5 = fil5(102:801,4);
fil6 = load('WD1FFT06.asc', '-ascii'); 
f6 = fil6(102:801,1);
m6 = fil6(102:801,4);
fil7 = load('WD1FFT07.asc', '-ascii'); 
f7 = fil7(202:801,1);
m7 = fil7(202:801,4);
fil8 = load('WD1FFT08.asc', '-ascii'); 
f8 = fil8(5:801,1);
m8 = fil8(5:801,4);

F1 = [f8;f7;f6;f5];
M1 = [m8;m7;m6;m5];
N1 = M1./10.4;

save('laser_noise','F1','M1','N1')
%% Watchdog after amp I data load
fil1 = load('WD2FFT01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('WD2FFT02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('WD2FFT03.asc', '-ascii'); 
f3 = fil3(202:801,1);
m3 = fil3(202:801,4);
fil4 = load('WD2FFT04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);

fil5 = load('WD2FFT30.asc', '-ascii'); 
f5 = fil5(102:801,1);
m5 = fil5(102:801,4);
fil6 = load('WD2FFT31.asc', '-ascii'); 
f6 = fil6(102:801,1);
m6 = fil6(102:801,4);
fil7 = load('WD2FFT32.asc', '-ascii'); 
f7 = fil7(202:801,1);
m7 = fil7(202:801,4);
fil8 = load('WD2FFT33.asc', '-ascii'); 
f8 = fil8(5:801,1);
m8 = fil8(5:801,4);

fil5 = load('WD2FFT20.asc', '-ascii'); 
fa1 = fil5(:,1);
ma1 = fil5(:,4);
fil6 = load('WD2FFT21.asc', '-ascii'); 
fa2 = fil6(:,1);
ma2 = fil6(:,4);
fil7 = load('WD2FFT22.asc', '-ascii'); 
fa3 = fil7(:,1);
ma3 = fil7(:,4);
fil8 = load('WD2FFT23.asc', '-ascii'); 
fa4 = fil8(:,1);
ma4 = fil8(:,4);
fil8 = load('WD2FFT24.asc', '-ascii'); 
fa5 = fil8(:,1);
ma5 = fil8(:,4);

F2 = [f4;f3;f2;f1];
M2 = [m4;m3;m2;m1];
N2 = M2./2.45;

F3 = [f8;f7;f6;f5];
M3 = [m8;m7;m6;m5];
N3 = M3./2;

figure()
loglog(F1,N1,'Color',c(1,:))
hold on
loglog(F2,N2,'Color',c(3,:))
loglog(F3,N3,'Color',c(4,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
title('Laser Intensity Noise on Watchdog PD')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('after amp II (flow rate=2l/min)','after amp I (flow rate=2l/min)','after amp I (flow rate=1l/min)')
