clear
load DEC04-02.asc

c = linspecer(1);

figure(1)
clf
axes('NextPlot','replacechildren','ColorOrder',c)
loglog(DEC04_02(:,1),DEC04_02(:,2))
ylabel('V_{rms}/\surd(Hz)')
xlabel('Frequency (Hz')
axis tight
print('4Hz_control_box_psu','-dpng')
