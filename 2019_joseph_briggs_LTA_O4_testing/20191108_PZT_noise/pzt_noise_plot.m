pzt_noise = srs780DataStitcher({importdata('PMCFB04.asc'),importdata('PMCFB03.asc'),importdata('PMCFB02.asc'),importdata('PMCFB01.asc')});

figure(1)
loglog(pzt_noise(:,1),pzt_noise(:,2))
xlabel('Frequency (hz)')
ylabel('V_{rms}/\surd(Hz)')
title('PMC PZT spectrum (HV mon)') 
axis tight
