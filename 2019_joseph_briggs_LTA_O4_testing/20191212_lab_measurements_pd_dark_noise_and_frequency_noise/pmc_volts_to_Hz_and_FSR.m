[t1, trace1] = importAgilentBin('scope_10_cal1.bin',1);
[t2, trace2] = importAgilentBin('scope_10_cal1.bin',2);
[t3, trace3] = importAgilentBin('scope_10_cal1.bin',3);

info = importAgilentBinDesc('scope_10_cal1.bin');


windowSize = 250; 
b = (1/windowSize)*ones(1,windowSize);
a = 1;
trace_1_padded = [ones(1000,1).*trace1(1);trace1];
trace1_padded_filter = filter(b,a,trace_1_padded);
trace1_filter = trace1_padded_filter(1001:end);

%trace 1 seems to be inverted??
t1_factor = 50;

%171 V = 150MHz, 1V = 150/171 MHz, times trace1 by this

Frequency_conversion = 150/171.*t1_factor.*trace1_filter;

N=1;
C =linspecer(N);

figure(1)
subplot(3,1,1)
plot(t1,t1_factor.*trace1_filter,'Color',C)
axis tight
ylabel('Amplitude (V)')


subplot(3,1,2)
plot(t2,trace2,'Color',C)
axis tight
ylabel('Amplitude (V)')

subplot(3,1,3)
plot(t3,trace3,'Color',C)
axis tight
xlabel('Time (ms)')
ylabel('Amplitude (V)')


figure(2)
subplot(2,1,1)
plot(Frequency_conversion-mean(Frequency_conversion),trace2,'Color',C)
axis tight
ylabel('Amplitude (V)')

subplot(2,1,2)
plot(Frequency_conversion-mean(Frequency_conversion),trace3,'Color',C)
axis tight
xlabel('Frequency (MHz)')
ylabel('Amplitude (V)')

