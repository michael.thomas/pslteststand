clear
test_3_to_mixer = load('DEC06-02.asc');
test_3_to_x28 = load('DEC06-03.asc');
gain_stage_0dB = load('DEC06-04.asc');
gain_stage_20dB = load('DEC06-05.asc');
gain_stage_40dB = load('DEC06-06.asc');
x10_x11 = load('DEC06-07.asc');
x11_x29 = load('DEC06-08.asc');
x11_x45 = load('DEC06-09.asc');
x16_x45 = load('DEC06-11.asc');
test_3_to_hv_mon = load('DEC06-10.asc');
openloop = load('DEC06-12.asc');

N = 11;
C = linspecer(N);

OL_start = 400;
OL_end = 800;
figure(1)
clf
subplot(2,1,1)
semilogx(test_3_to_mixer(:,1),test_3_to_mixer(:,4),'Color',C(1,:))
hold on
semilogx(test_3_to_x28(:,1),test_3_to_x28(:,4),'Color',C(2,:))
semilogx(gain_stage_0dB(:,1),gain_stage_0dB(:,4),'Color',C(3,:),'LineWidth',4)
semilogx(gain_stage_20dB(:,1),gain_stage_20dB(:,4),'Color',C(4,:))
semilogx(gain_stage_40dB(:,1),gain_stage_40dB(:,4),'Color',C(5,:))
semilogx(x10_x11(:,1),x10_x11(:,4),'Color',C(6,:))
semilogx(x11_x29(:,1),x11_x29(:,4),'Color',C(7,:),'LineStyle','--')
semilogx(x11_x45(:,1),x11_x45(:,4),'Color',C(8,:),'LineStyle',':','LineWidth',4)
semilogx(x16_x45(:,1),x16_x45(:,4),'Color',C(9,:),'LineStyle','-.')
semilogx(test_3_to_hv_mon(:,1),test_3_to_hv_mon(:,4),'Color',C(10,:))
semilogx(openloop(OL_start:OL_end,1),openloop(OL_start:OL_end,4),'Color',C(11,:))
hold off
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
axis tight
subplot(2,1,2)
semilogx(test_3_to_mixer(:,1),unwrap(test_3_to_mixer(:,5)),'Color',C(1,:))
hold on
semilogx(test_3_to_x28(:,1),test_3_to_x28(:,5),'Color',C(2,:))
semilogx(gain_stage_0dB(:,1),gain_stage_0dB(:,5),'Color',C(3,:),'LineWidth',4)
semilogx(gain_stage_20dB(:,1),gain_stage_20dB(:,5),'Color',C(4,:))
semilogx(gain_stage_40dB(:,1),gain_stage_40dB(:,5),'Color',C(5,:))
semilogx(x10_x11(:,1),unwrap(x10_x11(:,5),180),'Color',C(6,:))
semilogx(x11_x29(:,1),unwrap(x11_x29(:,5),180),'Color',C(7,:),'LineStyle','--')
semilogx(x11_x45(:,1),unwrap(x11_x45(:,5),180),'Color',C(8,:),'LineStyle',':','LineWidth',4)
semilogx(x16_x45(:,1),unwrap(x16_x45(:,5),180),'Color',C(9,:),'LineStyle','-.')
semilogx(test_3_to_hv_mon(:,1),test_3_to_hv_mon(:,5),'Color',C(10,:))
semilogx(openloop(OL_start:OL_end,1),openloop(OL_start:OL_end,5),'Color',C(11,:))
hold off
lgd = legend('4a','4b','4c 0dB','4c 20dB','4c 40dB','4d','4e','4f','4g','test 3 to HV mon','open loop','Location','southoutside');
set(lgd, 'NumColumns' ,12)
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
axis tight

print('transfer_functions','-dpng')
%%
[t,pdh_signal] = importAgilentBin('scope_5.bin',1);
[t,ramp] = importAgilentBin('scope_5.bin',2);
background_colour = [1 1 1].*0.1;
pdh_colour = [1 0.8 0.3];
ramp_colour = [1 0 0]'
grid_colour = [1 1 1];
minor_grid_colour = [1 1 1].*0.6;
figure(2)
yyaxis left
plot(t.*1000,pdh_signal.*1000,'Color',pdh_colour)
ylabel('Voltage (mV)')
yyaxis right
plot(t.*1000,ramp.*50,'Color',ramp_colour)
grid minor
set(gca,'Color',background_colour)
set(gca,'GridColor',grid_colour)
set(gca,'MinorGridColor',minor_grid_colour)
xlabel('Time (ms)')
ylabel('Voltage (V)')
set(gca,'FontSize',20)
lgd = legend('PDH signal','PZT','location','northeast');
set(lgd,'Color',[1 1 1])
print('pdh_error_signal','-dpng','-r0')


%%
[t,fsr] = importAgilentBin('scope_6.bin',1);
[t,ramp] = importAgilentBin('scope_6.bin',2);
background_colour = [1 1 1].*0.1;
pdh_colour = [1 0.8 0.3];
ramp_colour = [1 0 0]'
grid_colour = [1 1 1];
minor_grid_colour = [1 1 1].*0.6;
figure(3)
yyaxis left
plot(t.*1000,fsr.*1000,'Color',pdh_colour)
ylabel('Voltage (mV)')
axis tight
set(gca,'YColor',[0 0 0])
yyaxis right
plot(t.*1000,ramp.*50,'Color',ramp_colour)
axis tight
grid minor
set(gca,'Color',background_colour)
set(gca,'GridColor',grid_colour)
set(gca,'MinorGridColor',minor_grid_colour)
xlabel('Time (ms)')
ylabel('Voltage (V)')
set(gca,'FontSize',20)
lgd = legend('Transmission','PZT','location','northoutside');
set(lgd,'Color',[1 1 1],'NumColumns',2)
print('cavity scan','-dpng','-r0')
