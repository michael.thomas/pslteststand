load DEC02-01.asc
load DEC02-02.asc
load DEC02-03.asc
load DEC02-04.asc
load DEC02-05.asc
load DEC02-06.asc
load DEC02-07.asc
load DEC02-08.asc
load DEC02-09.asc

C = linspecer(2);
figure(1)
clf
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(DEC02_09(:,1),DEC02_09(:,2)./9.33,...
    DEC02_01(:,1),DEC02_01(:,2)./9.33)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('First Amplifier Laser','Photodiode Dark Noise','location','east')
axis tight
print('Dark_noise_plot','-dpng')

C = linspecer(5);
figure(2)
clf
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(DEC02_01(:,1),DEC02_01(:,2)./9.33,...
    DEC02_02(:,1),DEC02_02(:,2)./9.70,...
    DEC02_03(:,1),DEC02_03(:,2)./9.77,...
    DEC02_04(:,1),DEC02_04(:,2)./9.44,...
    DEC02_05(:,1),DEC02_05(:,2)./9.40)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('Amp FR = 0.8L/min, Diode box FR = 0.95L/min',...
    'Amp FR = 1.0L/min, Diode box FR = 0.95L/min',...
    'Amp FR = 1.2L/min, Diode box FR = 0.95L/min',...
    'Amp FR = 0.8L/min, Diode box FR = 1.2L/min',...
    'Amp FR = 0.8L/min, Diode box FR = 1.5L/min')

axis tight
print('flow_rate_plot','-dpng')

C = linspecer(2);
figure(3)
clf
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(DEC02_01(:,1),DEC02_01(:,2)./9.33,...
    DEC02_08(:,1),DEC02_08(:,2)./7.65)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('diode currents = 10.4A, 9.3A','diode currents = 8.4A, 8.3A','location','southwest')
axis tight
print('Diode_current_plot','-dpng')

figure(4)
figure(3)
clf
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(DEC02_01(:,1),DEC02_01(:,2)./9.33,...
    DEC02_06(:,1),DEC02_06(:,2)./2.55)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('First Amplifier Laser','Second Amplifier Laser','location','southwest')
axis tight
print('apmlifier_comparrison_plot','-dpng')