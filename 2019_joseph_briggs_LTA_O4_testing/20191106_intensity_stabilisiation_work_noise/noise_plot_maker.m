clear

laser_noise_low_f = srs780DataStitcher({importdata('PDA04.asc'),importdata('PDA03.asc'),importdata('PDA02.asc'),importdata('PDA01.asc')});
analyser_noise = srs780DataStitcher({importdata('PDAAN.asc')});
pda_dark_noise = srs780DataStitcher({importdata('PDADARK2.asc'),importdata('PDADARK.asc')});
%%

laser_high_f = importdata('LNOISEOF.txt');
laser_high_f_data = laser_high_f.data;
laser_high_f_voltage_noise = 10.^(laser_high_f_data(:,2)./20);

laser_noise_high_f = [laser_high_f_data(:,1),laser_high_f_voltage_noise];

%%
figure(1)
loglog(laser_noise_low_f(:,1),laser_noise_low_f(:,2),...
    pda_dark_noise(:,1),pda_dark_noise(:,2),...
    analyser_noise(:,1),analyser_noise(:,2))
xlabel('Frequency (Hz)')
ylabel('Vrms/\surd(Hz)')
legend('Laser Noise','Photodiode Dark Noise','Analyser Noise')
axis tight
print('laser_noise_low_f','-dpng')

figure(3)
loglog(laser_noise_low_f(:,1),laser_noise_low_f(:,2)./10.2)
xlabel('Frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('Laser Noise')
axis tight
print('laser_noise_low_f_RIN','-dpng')


%%
% figure(2)
% loglog(laser_high_f_data(:,1),laser_high_f_data(:,2))
% xlabel('Frequency (Hz)')
% ylabel('dBV/Hz')
% axis tight
% legend('Laser Noise')
% print('laser_noise_high_f','-dpng')