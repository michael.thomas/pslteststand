clear
load ANALYSER.asc
load DARK.asc
load LIGHT.asc

load WDDARK01.asc
load WDDARK02.asc
load FANOFF.asc
load FANON.asc

NPRO_DC = 2.57;
WD_DC = 9.58;

N = 8;
c = linspecer(N);

figure(1)
loglog(ANALYSER(:,1),ANALYSER(:,2)./NPRO_DC,'Color',c(1,:))
hold on
loglog(DARK(:,1),DARK(:,2)./NPRO_DC,'Color',c(2,:))
loglog(LIGHT(:,1),LIGHT(:,2)./NPRO_DC,'Color',c(3,:))
loglog(WDDARK01(:,1),WDDARK01(:,2)./WD_DC,'color',c(4,:))
loglog(FANON(:,1),FANON(:,2)./WD_DC,'color',c(6,:))
loglog(FANOFF(:,1),FANOFF(:,2)./WD_DC,'color',c(7,:))
hold off
xlabel('frequency (Hz)')
ylabel('RIN/\surd(Hz)')
legend('Analyser Noise (wrt to NPRO)','NPRO Photodiode Dark Noise','NPRO RIN',...
    'Amp ii watchdog dark noise (beam dump)','Amp ii fans on','Amp ii fans off')
axis([0.5 200 1e-9 9.9e-2])
set(gca,'FontSize',20')
print('noise hunting','-dpng','-r0')