clear all

load pmc_weekend_old

pmc_trans_old = min_trend_data(1).data; %Watts
pmc_ref_old = min_trend_data(2).data; %Watts
pmc_sum_old = min_trend_data(3).data; %Watts
time_old = 0:1:length(min_trend_data(1).data)-1; %time series in steps of the data rate (1 sample/min)
time_hour_old = time_old./60; %hours
humidity_old = min_trend_data(11).data;
amp_i_temperature_old = min_trend_data(13).data;

amp_ii_new = importdata('amp_ii');
amp_i_new = importdata('amp_i.asc');
pmc_trans_new = importdata('pmc_trans');
pmc_reflected_new  = importdata('pmc_relected');
pmc_sum_new  = importdata('pmc_sum');
pmc_temp_new = importdata('pmc_temp');
humidity_new = importdata('humidity');
amp_i_temp_new = importdata('amp_i_temp.asc');
room_temp_new = importdata('room_temp');

time = (amp_i_new(:,1)-amp_i_new(1,1))./3600; %hour

N = 5;  
c = linspecer(N);


figure(1)

clf
axes('NextPlot','replacechildren', 'ColorOrder',c);
plot(time,amp_i_new(:,2))
hold on
plot(time,amp_ii_new(:,2))
plot(time,pmc_sum_new(:,2))
plot(time,pmc_trans_new(:,2))
set(gca,'YLim',[67 127]);
set(gca,'XLim',[0 56]);
grid on 
yticks(0:2:140)
set(gca,'FontSize',19);
lgd = legend('Second Amplifier','First Amplifier','PMC sum','PMC trans');
xlabel('Time (hour)')
ylabel('Laser Power (Watt)')
breakyaxis([69.5 105]);
legend('show')
hold off
%%

pos1 = [0.1 0.25 0.8 0.7];
pos2 = [0.1 0.1 0.8 0.1];

figure(2)
clf
axes('NextPlot','replacechildren', 'ColorOrder',c);
subplot('Position',pos1)
plot(time,pmc_reflected_new(:,2),'Color',c(1,:))
hold on
plot(time_hour_old,pmc_ref_old,'Color',c(1,:),'LineStyle','--')
plot(time,pmc_sum_new(:,2),'Color',c(2,:))
plot(time_hour_old,pmc_sum_old,'Color',c(2,:),'LineStyle','--')
plot(time,pmc_trans_new(:,2),'Color',c(3,:))
plot(time_hour_old,pmc_trans_old,'Color',c(3,:),'LineStyle','--')
hold off
set(gca,'YLim',[8 128]);
set(gca,'XLim',[0 56]);
grid on 
yticks(0:2:140)
set(gca,'xticklabel',[])
set(gca,'FontSize',19);
lgd = legend('PMC ref weekend 1','PMC ref weekend 2',...
    'PMC Sum weekend 1','PMC Sum weekend 2',...
    'PMC Trans weekend 1','PMC Trans weekend 2');
%xlabel('Time (hour)')
ylabel('Laser Power (Watt)')
breakyaxis([22 105]);
legend('show')

subplot('Position',pos2)
yyaxis left
plot(time,humidity_new(:,2),'Color',c(4,:))
hold on
plot(time_hour_old,humidity_old,'Color',c(4,:),'LineStyle','--')
hold off
ylabel({'TnT Lab','Humidity (%)'})
ax = gca;
ax.YColor = c(4,:);
ax.GridColor = [0 0 0];
yyaxis right
plot(time,amp_i_temp_new(:,2),'Color',c(5,:))
%hold on
%plot(time_hour_old,amp_i_temperature_old,'Color',c(5,:),'LineStyle','--')
%hold off
ylabel({'TnT Lab','Temperature (�C)'})
ax = gca;
ax.YColor = c(5,:);
axis tight
set(gca,'FontSize',18)
xlabel('Time (hour)')

%%

figure(3)

clf
axes('NextPlot','replacechildren', 'ColorOrder',c);
plot(time,pmc_sum_new(:,2))
hold on
plot(time,pmc_trans_new(:,2))
hold off
grid on
axis tight
xlabel('Time (hour)')
ylabel('Laser Power (Watt)')
legend('PMC Sum','PMC Trans','location','west')
grid minor
set(gca,'FontSize',19);

%%

figure(4)
subplot(3,1,1)
plot(time,pmc_trans_new(:,2),'Color',c(1,:))
axis tight
set(gca,'FontSize',18)
set(gca,'xticklabel',[])
ylabel({'PMC transmission',' (Watts)'})

subplot(3,1,2)
plot(time,humidity_new(:,2),'Color',c(1,:))
axis tight
set(gca,'FontSize',18)
set(gca,'xticklabel',[])
ylabel({'TnT Lab','Humidity (%)'})

subplot(3,1,3)
plot(time,amp_i_temp_new(:,2),'Color',c(1,:))
axis tight
set(gca,'FontSize',18)
ylabel({'Second Amplifier','Temperature (�C)'})
xlabel('Time (hour)')

