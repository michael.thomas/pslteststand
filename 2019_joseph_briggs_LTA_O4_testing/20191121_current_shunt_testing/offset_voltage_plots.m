offset = [1,0.8,0.6,0.4,0.2,0];

watchdog = [2.66,2.526,2.488,2.55,2.71,2.87];
DBB =[9.28,9.39,9.48,9.58,9.67,9.75];
iss_1 = [10.1,10.28,10.21,10.34,10.22,10.8];

N = 3;
c = linspecer(4);

figure(1)
clf
scatter(offset,watchdog./watchdog(end),200,'o','MarkerFaceColor',c(1,:),'MarkerEdgeColor',c(1,:))
hold on
scatter(offset,DBB./DBB(end),200,'d','MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:))
scatter(offset,iss_1./iss_1(end),200,'p','MarkerFaceColor',c(3,:),'MarkerEdgeColor',c(3,:))
hold off
legend('Watchdog PD','DBB PD','ISS PD')
xlabel('I Mod In votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
print('dc_power_all_pd','-dpng')

figure(2)
clf
scatter(offset,DBB./DBB(end),200,'d','MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:))
legend('DBB PD')
xlabel('I Mod In Votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
print('dc_power_dbb_pd','-dpng')

%%
iss_2 = [10.3,10.36,10.35,10.28,10.17,9.98];
pmc_t = [112.2,112.4,112.7,111.6,110.5,108.9];
pmc_r = [9.4,10.3,11.5,13.9,15.5,18.3];
pmc_sum = pmc_r+pmc_t;

figure(3)
clf
subplot(2,2,1)
scatter(offset,iss_2./iss_2(end),200,'o','MarkerFaceColor',c(1,:),'MarkerEdgeColor',c(1,:))
title('ISS PD')
xlabel('I Mod In votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
subplot(2,2,3)
scatter(offset,pmc_t./pmc_t(end),200,'d','MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:))
title('PMC transmitted')
xlabel('I Mod In votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
subplot(2,2,4)
scatter(offset,pmc_r./pmc_r(end),200,'p','MarkerFaceColor',c(3,:),'MarkerEdgeColor',c(3,:))
title('PMC reflected')
xlabel('I Mod In votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
subplot(2,2,2)
scatter(offset,pmc_sum./pmc_sum(end),200,'^','MarkerFaceColor',c(4,:),'MarkerEdgeColor',c(4,:))
title('PMC Sum')
xlabel('I Mod In votlage (V)')
ylabel('Relative Power')
set(gca,'FontSize',14)
print('dc_power_all_pd','-dpng')