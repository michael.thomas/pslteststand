%%
clear 

load TF-98-92.asc
load OFF400.asc
load TF-94-88.asc
load TF-92-86.asc
load TF-90-84.asc
load TF-88-82.asc
load TF-86-80.asc

DC_level_98_92 = 20.*log10(10.23);
DC_level_OFF400 = 20.*log10(10.32);
DC_level_94_88 = 20.*log10(10.27);
DC_level_92_86 = 20.*log10(10.07);
DC_level_90_84 = 20.*log10(9.80);
DC_level_88_82 = 20.*log10(9.42);
DC_level_86_80 = 20.*log10(9.89);

%%
N = 7;

c = linspecer(N);

figure(1)
clf
subplot(2,1,1)
%axes('NextPlot','replacechildren', 'ColorOrder',C);
semilogx(TF_98_92(:,1),TF_98_92(:,4)-DC_level_98_92,'Color',c(1,:))
hold on
semilogx(OFF400(:,1),OFF400(:,4)-DC_level_OFF400,'Color',c(2,:))
semilogx(TF_94_88(:,1),TF_94_88(:,4)-DC_level_94_88,'Color',c(3,:))
semilogx(TF_92_86(:,1),TF_92_86(:,4)-DC_level_92_86,'Color',c(4,:))
semilogx(TF_90_84(:,1),TF_90_84(:,4)-DC_level_90_84,'Color',c(5,:))
semilogx(TF_88_82(:,1),TF_88_82(:,4)-DC_level_88_82,'Color',c(6,:))
semilogx(TF_86_80(:,1),TF_86_80(:,4)-DC_level_86_80,'Color',c(7,:))
hold off
xlabel('Frequency (Hz)')
ylabel('Gain dB')
axis tight
legend({'Diode 1/2: 9.8A, Diode 3/4: 9.2',...
    'Diode 1/2: 9.6A, Diode 3/4: 9.0',...
    'Diode 1/2: 9.4A, Diode 3/4: 8.8',...
    'Diode 1/2: 9.2A, Diode 3/4: 8.6',...
    'Diode 1/2: 9.0A, Diode 3/4: 8.4',...
    'Diode 1/2: 8.8A, Diode 3/4: 8.2',...
    'Diode 1/2: 8.6A, Diode 3/4: 8.0'},'EdgeColor',[1 1 1])
set(gca,'FontSize',14)

subplot(2,1,2)
semilogx(TF_98_92(:,1),TF_98_92(:,5),'Color',c(1,:))
hold on
semilogx(OFF400(:,1),OFF400(:,5),'Color',c(2,:))
semilogx(TF_94_88(:,1),TF_94_88(:,5),'Color',c(3,:))
semilogx(TF_92_86(:,1),TF_92_86(:,5),'Color',c(4,:))
semilogx(TF_90_84(:,1),TF_90_84(:,5),'Color',c(5,:))
semilogx(TF_88_82(:,1),TF_88_82(:,5),'Color',c(6,:))
semilogx(TF_86_80(:,1),TF_86_80(:,5),'Color',c(7,:))
hold off
xlabel('Frequency (Hz)')
ylabel('Phase (Degree)')
axis tight
set(gca,'FontSize',14)