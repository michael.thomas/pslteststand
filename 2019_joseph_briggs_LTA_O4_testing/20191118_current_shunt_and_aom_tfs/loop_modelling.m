clear
%tf_measured = load('CS401.asc');

tf_measured = load('CSTF01.asc');

start_bin = 1;
f = tf_measured(start_bin:end,1);
tf_mag = tf_measured(start_bin:end,4);
tf_real = tf_measured(start_bin:end,2);
tf_imag = tf_measured(start_bin:end,3);
tf_phase = unwrap(tf_measured(start_bin:end,5),180)+360;
fitting_data = [tf_real,tf_imag];
x0 = [4.5e4,-0.027,7,10,100,150,5200,7000];
%x0 = [1,3,4,10,100,200,1000,4500,7000,0.07];
x = lsqcurvefit(@tf_fit_func,x0,f,fitting_data);


[a,b,c,d] = lowpass(x0(1),x0(2));
lp = ss(a,b,c,d);
[a,b,c,d] = transdif(x0(3),x0(4),1);
hp = ss(a,b,c,d);
[a,b,c,d] = transint(x0(5),x0(6),1);
ti = ss(a,b,c,d);
[a,b,c,d] = transdif(x0(7),x0(8),1);
td = ss(a,b,c,d);
tf_guess = series(td,series(ti,series(hp,series(lp,hp))));

[a,b,c,d] = lowpass(x(1),x(2));
lp = ss(a,b,c,d);
[a,b,c,d] = transdif(x(3),x(4),1);
hp = ss(a,b,c,d);
[a,b,c,d] = transint(x(5),x(6),1);
ti = ss(a,b,c,d);
[a,b,c,d] = transdif(x(7),x(8),1);
td = ss(a,b,c,d);
fitted_model = series(td,series(ti,series(hp,series(lp,hp))));

[mag_guess,phase_guess] = bode(tf_guess,f.*2.*pi);
mag_guess = 20.*log10(squeeze(mag_guess));
phase_guess = squeeze(phase_guess);

[mag_fit,phase_fit] = bode(fitted_model,f.*2.*pi);
mag_fit = 20.*log10(squeeze(mag_fit));
phase_fit = squeeze(phase_fit);

figure(1)
subplot(2,1,1)
semilogx(f,tf_mag,f,mag_guess)
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
axis tight
subplot(2,1,2)
semilogx(f,tf_phase,f,phase_guess)
xlabel('Frequency (Hz)')
ylabel('phase (degrees)')
axis tight
legend('measured','model')
