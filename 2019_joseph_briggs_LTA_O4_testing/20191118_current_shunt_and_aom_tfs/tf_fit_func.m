function [tf_out,tf_guess] = tf_fit_func(x,f)

    [a,b,c,d] = lowpass(x(1),x(2));
    lp = ss(a,b,c,d);
    [a,b,c,d] = transdif(x(3),x(4),1);
    hp = ss(a,b,c,d);
    [a,b,c,d] = transint(x(5),x(6),1);
    ti = ss(a,b,c,d);
    [a,b,c,d] = transdif(x(7),x(8),1);
    td = ss(a,b,c,d);
    
    tf_guess = series(lp,hp);
    tf_guess = series(tf_guess,ti);
    tf_guess = series(tf_guess,td);
%     

%     [a,b,c,d] = transint(x(1),x(2),1);
%     ti_1 = ss(a,b,c,d);
%     [a,b,c,d] = transdif(x(3),x(4),1);
%     td_1 = ss(a,b,c,d);
%     [a,b,c,d] = transint(x(5),x(6),1);
%     ti_2 = ss(a,b,c,d);
%     [a,b,c,d] = transdif(x(7),x(8),1);
%     td_2 = ss(a,b,c,d);
%     [a,b,c,d] = lowpass(x(9),x(10));
%     lp = ss(a,b,c,d);
%     tf_guess = series(ti_1,td_1);
%     tf_guess = series(tf_guess,ti_2);
%     tf_guess = series(tf_guess,td_2);
%     tf_guess = series(tf_guess,lp);

    [guess_real,guess_imag] = nyquist(tf_guess,f.*2.*pi);
    mag_guess = squeeze(guess_real);
    imag_guess = squeeze(guess_imag);
    tf_out = [mag_guess,imag_guess];

end

