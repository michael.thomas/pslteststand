[time,voltage] = importAgilentBin('scan_pmc061123.bin');

figure(1)
plot(time,voltage)
xlabel('time (s)')
ylabel('Reflected light voltage (V)')
title('PMC scan')
print('pmc_reflection')