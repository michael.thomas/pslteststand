PMCUG = importdata('PMCUG.asc');

c = linspecer(2);

start_bin = 175;
end_bin = 600;

[val,index] = min(abs(20.*log10(PMCUG(start_bin:end_bin,4))));

ugp_frequency = PMCUG(start_bin + index,1);
ugp_phase = PMCUG(start_bin + index,5);

figure(1)
clf
subplot(2,1,1)
plot(PMCUG(start_bin:end_bin,1),20.*log10(PMCUG(start_bin:end_bin,4)),...
    '-p','MarkerIndices',index,'MarkerSize',15,'MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:),'Color',c(1,:))
set(gca,'XScale','log')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
axis tight
set(gca, 'FontSize', 12)

subplot(2,1,2)
semilogx(PMCUG(start_bin:end_bin,1),PMCUG(start_bin:end_bin,5),...
    '-p','MarkerIndices',index,'MarkerSize',15,'MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:),'Color',c(1,:))
axis tight
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
set(gca, 'FontSize', 12)
dim = [.2 .2 .2 .2];
str = ['Unity gain point at ', num2str(ugp_frequency,'%.0f'),' Hz with phase ', num2str(ugp_phase,'%.0f'),' degrees'];
annotation('textbox',dim,'String',str,'FontSize',12,'FitBoxToText','on');

print('PMC_open_loop_transfer_function','-dpng','-r0')