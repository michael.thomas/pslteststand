clear
pda_dark_noise = srs780DataStitcher({importdata('PDADARK2.asc'),importdata('PDADARK.asc')});
disp('loaded dark noise data...')
%% ND filter data
DC_ND = 10.7; %Volts, with ND filter
pda_laser_noise_high_flow_fans_on = srs780DataStitcher({importdata('PDAFFT05.asc'),importdata('PDAFFT04.asc'),importdata('PDAFFT02.asc'),importdata('PDAFFT01.asc')}); %Vrms/sqt(Hz)
pda_laser_noise_high_flow_fans_off = srs780DataStitcher({importdata('PDAFFT09.asc'),importdata('PDAFFT08.asc'),importdata('PDAFFT07.asc'),importdata('PDAFFT06.asc')}); %Vrms/sqt(Hz)

disp('loaded_big spectra...')
%%
pda_laser_noise_low_flow_fans_on = importdata('PDAFFT10.asc'); %Vrms/sqt(Hz)
pda_laser_noise_low_flow_fans_off = importdata('PDAFFT11.asc');

pda_laser_noise_extra_low_flow_fans_on = importdata('PDAFFT13.asc');
pda_laser_noise_extra_low_flow_fans_off = importdata('PDAFFT13.asc');

disp('loaded ND data...')
%% HWP without tp 1%

DC_HWP = 11.26;
pda_laser_HWP = importdata('PDAFFT15.asc');
disp('loaded HWP data...')
%% HWP with tp 1%
DC_TP = 9.45;
pda_laser_noise_TP_fans_on = importdata('PDAFFT16.asc');
pda_laser_noise_TP_fans_off = importdata('PDAFFT17.asc');
disp('loaded Tp = 1% data...')
%% requirement
very_low_f = linspace(1,10,100);
requirement_very_low_f = 2e-9.*100.*very_low_f.^-2;
low_f = linspace(10,100,100);
requirement_low_f = 2e-10.*low_f;
med_f = linspace(100,1000);
requirement_med_f = 2e-8.*ones(1,length(med_f));
high_f = linspace(1e3,1e4);
requirement_high_f = 2e-8./1000.*high_f;
very_high_f = linspace(1e4,1e5);
requirement_very_high_f = 2e-7.*ones(1,length(very_high_f));

f = [very_low_f,low_f,med_f,high_f,very_high_f];
requirement = [requirement_very_low_f,requirement_low_f,requirement_med_f,requirement_high_f,requirement_very_high_f];
%% typical noise


%figure(2)
%loglog(f,requirement)

%%
figure(1)
clf
C = linspecer(10);
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(pda_laser_noise_high_flow_fans_on(:,1),pda_laser_noise_high_flow_fans_on(:,2)./DC_ND,...
    pda_laser_noise_high_flow_fans_off(:,1),pda_laser_noise_high_flow_fans_off(:,2)./DC_ND,...
    pda_laser_noise_low_flow_fans_on(:,1),pda_laser_noise_low_flow_fans_on(:,2)./DC_ND,...
    pda_laser_noise_low_flow_fans_off(:,1),pda_laser_noise_low_flow_fans_off(:,2)./DC_ND,...
    pda_laser_noise_extra_low_flow_fans_on(:,1),pda_laser_noise_extra_low_flow_fans_on(:,2)./DC_ND,...
    pda_laser_noise_extra_low_flow_fans_off(:,1),pda_laser_noise_extra_low_flow_fans_off(:,2)./DC_ND,...
    pda_laser_HWP(:,1),pda_laser_HWP(:,2)./DC_HWP,...
    pda_laser_noise_TP_fans_on(:,1),pda_laser_noise_TP_fans_on(:,2)./DC_TP,...
    pda_laser_noise_TP_fans_off(:,1),pda_laser_noise_TP_fans_off(:,2)./DC_TP,...
    f,requirement)
    
xlabel('Frequency (Hz)','Fontsize',24)
ylabel('RIN/\surd(Hz)','Fontsize',24)
legend('Flow rate amp ii = 1L/min, amp i = 2L/min, HEPA fans on, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 2L/min, HEPA fans off, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 1L/min, HEPA fans on, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 1L/min, HEPA fans off, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 0.8L/min, HEPA fans on, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 0.8L/min, HEPA fans off, ND filter used',...
    'Flow rate amp ii = 1L/min, amp i = 1L/min, HEPA fans on, HWP/polariser',...
    'Flow rate amp ii = 1L/min, amp i = 1L/min, HEPA fans on, TP = 1% + HWP/polariser',...
    'Flow rate amp ii = 1L/min, amp i = 1L/min, HEPA fans off, TP = 1% + HWP/polariser','location','southwest',...
    'aLIGO design Requirement T0900649-v4')
axis([30 6.4e3 1e-8 1e-4])
%%
figure(3)
clf
C = linspecer(3);
axes('NextPlot','replacechildren', 'ColorOrder',C);
loglog(pda_laser_HWP(:,1),pda_laser_HWP(:,2)./DC_HWP,...
    pda_laser_noise_TP_fans_off(:,1),pda_laser_noise_TP_fans_off(:,2)./DC_TP)
    
xlabel('Frequency (Hz)','Fontsize',24)
ylabel('RIN/\surd(Hz)','Fontsize',24)
legend('After PMC, fans on','After PMC, fans off')
axis([30 6.4e3 1e-7 1e-4])
