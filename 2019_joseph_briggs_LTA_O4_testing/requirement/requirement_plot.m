
f = logspace(-1,5,5000);
requirement = zeros(1,length(f));

idx = 0;
for freq = f
    idx = idx +1;  
    if freq < 2e-1
        requirement(idx) = 1e-2;
    elseif freq >= 2e-1 && freq < 5e-1
        requirement(idx) = freq^-7*1.25e-7;
    elseif freq >=5e-1 && freq < 10
        requirement(idx) = freq^-3*2e-6;
    elseif freq >= 10 && freq < 2e3
        requirement(idx) = freq*2e-10;
    else
        requirement(idx) = 4e-7;
    end    
end 

figure(100)
loglog(f,requirement,'.')
axis tight
xlabel('Frequency (Hz)')
ylabel('Requirement RIN/\surd(Hz)')