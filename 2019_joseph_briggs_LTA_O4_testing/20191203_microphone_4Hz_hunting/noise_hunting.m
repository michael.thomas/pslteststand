load DEC03-01.asc
load DEC03-02.asc
load DEC03-03.asc
load DEC03-04.asc
load DEC03-05.asc
load DEC03-06.asc
load DEC03-07.asc
load DEC03-08.asc

C = linspecer(6);

start_bin = 9;

fan_off_noise = srs780DataStitcher({DEC03_02,DEC03_07});
fan_on_noise = srs780DataStitcher({DEC03_01,DEC03_08});

figure(1)
clf
axes('NextPlot','replacechildren','ColorOrder',C)
loglog(DEC03_01(start_bin:end,1),DEC03_01(start_bin:end,2),...
    DEC03_01(start_bin:end,1),DEC03_02(start_bin:end,2),...
    DEC03_01(start_bin:end,1),DEC03_03(start_bin:end,2),...
    DEC03_01(start_bin:end,1),DEC03_04(start_bin:end,2),...
    DEC03_01(start_bin:end,1),DEC03_05(start_bin:end,2),...
    DEC03_01(start_bin:end,1),DEC03_06(start_bin:end,2))
axis tight

xlabel('Frequency (Hz)')
ylabel('V_{rms}/\surd(Hz)')

legend('DEC03-01, HEPA fans on',...
    'DEC03-02, HEPA fans off',...
    'DEC03-03, Beckhoff Computer',...
    'DEC03-04, Near Amp II head',...
    'DEC03-05, Near Amp II diode box',...
    'DEC03-06, Pointed at HEPA fans','Location','best')
print('microphone_plot','-dpng')

figure(2)
clf
axes('NextPlot','replacechildren','ColorOrder',C)
loglog(fan_on_noise(:,1),fan_on_noise(:,2),...
    fan_off_noise(:,1),fan_off_noise(:,2))
axis tight
xlabel('Frequency (Hz)')
ylabel('V_{rms}/\surd(Hz)')  
legend('HEPA Fans on','HEPA Fans off')
print('microphone_plot_2','-dpng')
