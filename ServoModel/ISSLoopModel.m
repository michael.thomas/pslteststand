close all
clear all
MP2complex = @(m,p)m(:).*cosd(p(:)) + m(:).*sind(p(:)).*1i;

% cc=linspecer(9);
%% Load LISO Files
% TF of PD circuit FILT output D1001998
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD = LISOPD.data(:,2);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(10.^(gPD./20),pPD);

%TF of ISS Servo Board D1001985
%ISSTF00.out = Unmodified board
%ISSTF01.out = Modified servo board for current shunt including phase lead and x50 gain
%ISSTF02.out = Modified board with integrators OFF
LISOISS = importdata('ISSTF03.out');
freq = LISOISS.data(:,1);
gain = LISOISS.data(:,2);
phas = LISOISS.data(:,3);
ServoTF = MP2complex(10.^(gain./20),phas);

gPD0 = interp1(fPD,gPD,freq);
PDTF = interp1(fPD,PDTF0,freq);
ISSTF = ServoTF.*PDTF;  %Electronic Servo TF, Board + PD
% ISSTF = PDTF;  %Electronic Servo TF PD
gainISS = 20.*log10(abs(ISSTF));
phasISS = angle(ISSTF).*(180./pi);

%% Load .mat files
CSdata = load('CSresp3.mat');   %Current shunt TF response (measured data)
fCS = CSdata.CSresp2(:,1);
gCS = 10.^(CSdata.CSresp2(:,2)./20);
pCS = CSdata.CSresp2(:,3);
CSTF0 = MP2complex(gCS,pCS);
CSTF = interp1(fCS,CSTF0,freq);
[a1,b1,c1,d1] = transdif(0.9e4,2.1e4,2.7);
[a2,b2,c2,d2] = lowpass(2.4e4,1);
sys = series(ss(a1,b1,c1,d1),ss(a2,b2,c2,d2));

% [a,b,c,d] = res2pole(1e4,0.1,300);
[gLP,pLP] = bode(sys,freq);
gLP=gLP(:);
pLP=pLP(:);
chig = sum((interp1(freq,gLP,fCS(fCS>1e3))-gCS(fCS>1e3)).^2)
chip = sum((interp1(freq,pLP,fCS(fCS>1e3))-pCS(fCS>1e3)).^2)

figure()
subplot(211)
loglog(freq,gLP)
hold on
loglog(fCS,gCS)
loglog(fCS,gCS-(interp1(freq,gLP,fCS)),'--')
loglog([100,100e3],[gLP(1)*sqrt(2),gLP(1)*sqrt(2)],'k:')
loglog([100,100e3],[gLP(1)/sqrt(2),gLP(1)/sqrt(2)],'k:')

xlim([100,100e3])
ylim([1,5])

subplot(212)
semilogx(freq,pLP)
hold on
semilogx(fCS,pCS)
xlim([100,100e3])


AOMdata = load('AOMTF.mat');    %AOM TF response (measured data)
fAOM = AOMdata.AOMresp1(:,1);
gAOM = AOMdata.AOMresp1(:,2);
pAOM = AOMdata.AOMresp1(:,3);
AOMTF0 = MP2complex(gAOM,pAOM);
AOMTF = interp1(fAOM,AOMTF0,freq);


AOMdata1 = load('AOMfit.mat');  %AOM TF response (fit to measurement)
fAOM1 = AOMdata1.AOMfit(:,1);
gAOM1 = 10.^(AOMdata1.AOMfit(:,2)./20);
pAOM1 = AOMdata1.AOMfit(:,3);
AOMTF01 = MP2complex(gAOM1,pAOM1);
AOMTF1 = interp1(fAOM1,AOMTF01,freq);

%LoopTFA is the loop transfer function with the AOM actuator
LoopTFA  = -ISSTF.*AOMTF1;
gainA = 20.*log10(abs(LoopTFA));
phasA = angle(LoopTFA).*(180./pi);

%LoopTFC is the loop transfer function with the Current Shunt actuator
% LoopTFC  = ServoTF.*CSTF;
LoopTFC  = -ISSTF.*CSTF;
gainC = 20.*log10(abs(LoopTFC));
phasC = angle(LoopTFC).*(180./pi);

%% Load OLG Data measured with AOM locking
fil2 = load('AOMOLG1.asc', '-ascii'); 
f5 = fil2(:,1);
g5 = fil2(:,4);
p5 = fil2(:,5);
LTF = MP2complex(10.^(g5./20),p5);

%% Load OLG Data measured with Current Shunt locking
fil2 = load('OLG33DBI.asc', '-ascii'); 
f6 = fil2(:,1);
g6 = fil2(:,4);
p6 = fil2(:,5);
LTF2 = MP2complex(10.^(g6./20),p6);

%% Loop Gain Factor
Gain = 33; %Servo gain set in medm screen in dB
PD_DC = 5.5; %DC Voltage on ISS PD in Volts
PWR = 139; %Laser power put of amp i
Gfact = (Gain+20.*log10(PD_DC./PWR));
gainCC = gainC+Gfact;

UGF = freq(find(gainCC<0,1));
if isempty(UGF)
    UGF=1;
end
PM = phasC(find(gainCC<0,1));
if isempty(PM)
    PM=0;
end
css = [0.8,0.8,0,0.4];
%% Plot
figure()
subplot(211)
semilogx(freq,gain,'m')
hold on
semilogx(fPD,gPD,'c')
semilogx(fCS,20.*log10(gCS),'r')
semilogx(freq,gainISS,'b')
semilogx(freq,Gfact.*ones(1,length(freq)),'k:')
% semilogx(f5,g5,'Color',[0.7,0.8,0.96,0.9],'LineWidth',4)
semilogx(f6,g6,'Color',[0.7,0.98,0.6,0.9],'LineWidth',4)
semilogx(freq,gainCC,'k')
semilogx([min(freq),max(freq)],[0,0],'-.','Color',css)
semilogx([UGF,UGF],[min(min(gain,gPD))-10,max(max(gainCC,gPD))+10],'-.','Color',css)

grid on
xlim([min(freq),max(freq)])
ylim([min(min(gain,gPD))-10,max(max(gainCC,gPD))+10])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('ISS Board (LISO)','PD FILT (LISO)','Current Shunt (data)','Gain factor','Loop(Model)')
% legend('ISS Board(LISO)','Current Shunt(data)','Gain factor','Loop(Data)','Loop(Model)')
subplot(212)
semilogx(freq,phas,'m')
hold on
semilogx(fPD,pPD,'c')
semilogx(fCS,pCS,'r')
semilogx(freq,phasISS,'b')
% semilogx(f5,p5,'Color',[0.7,0.8,0.96,0.9],'LineWidth',4)
semilogx(f6,p6,'Color',[0.7,0.98,0.6,0.9],'LineWidth',4)
semilogx(freq,phasC,'k')

semilogx([min(freq),max(freq)],[PM,PM],'-.','Color',css)
semilogx([UGF,UGF],[-180,180],'-.','Color',css)

grid on
ylim([-180,180])
xlim([min(freq),max(freq)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

% figure()
% subplot(211)
% semilogx(freq,gain,'m')
% hold on
% semilogx(fPD,gPD,'c')
% semilogx(fAOM1,20.*log10(gAOM1),'r')
% % semilogx(freq,gainISS,'b')
% semilogx(freq,Gfact.*ones(1,length(freq)),'k:')
% semilogx(f5,g5,'Color',[0.7,0.8,0.96,0.9],'LineWidth',4)
% semilogx(freq,gainA+Gfact,'k')
% 
% grid on
% xlim([min(freq),max(freq)])
% xlabel('Frequency (Hz)')
% ylabel('Gain (dB')
% title('Model of ISS Loop with AOM')
% legend('ISS Board(LISO)','PD FILT(LISO)','AOM(fit)','Gain factor','Loop(Data)','Loop(Model)')
% subplot(212)
% semilogx(freq,phas,'m')
% hold on
% semilogx(fPD,pPD,'c')
% semilogx(fAOM1,pAOM1,'r')
% % semilogx(freq,phasISS,'b')
% semilogx(f5,p5,'Color',[0.7,0.8,0.96,0.9],'LineWidth',4)
% semilogx(freq,phasA,'k')
% 
% grid on
% ylim([-180,180])
% xlim([min(freq),max(freq)])
% xlabel('Frequency (Hz)')
% ylabel('Phase (deg)')