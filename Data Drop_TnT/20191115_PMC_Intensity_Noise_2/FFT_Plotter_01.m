close all
clear all
c=linspecer(9);
%%
off = [0,-100,-200,-300,-400,-500,-600];
PMCsum = [128,128,127,126.5,126.2,125.5,125];
PMCTrans = [107,108,109,109,110,110.5,110.6];
PMCRefl = PMCsum-PMCTrans;
PMCEff = (PMCTrans./PMCsum).*100;


figure()
subplot(121)
plot(off,PMCTrans,'o--','Color',c(1,:))
hold on
plot(off,PMCRefl,'o--','Color',c(4,:))
plot(off,PMCsum,'o--','Color',c(5,:))
grid on
xlabel('PMC INOFFSET (mV)')
ylabel('Laser Power (W)')
legend('Transmission','Reflection','Sum')

subplot(122)
plot(off,PMCEff,'o--','Color',c(8,:))
grid on
xlabel('PMC INOFFSET (mV)')
ylabel('PMC Transmission Efficiency (%)')

%%
off2 = [0,100,200,300,400,500,600,700,800];
PMCsum2 = [128, 127.3, 126.6, 126.1, 125.6, 124.9, 124.5, 123.6, 122.9];
PMCTrans2 = [110.4, 111.1, 111.9, 112.5, 112.9, 113.1, 113.2, 113.1, 112.8];
PMCRefl2 = PMCsum2-PMCTrans2;
PMCEff2 = (PMCTrans2./PMCsum2).*100;


figure()
subplot(121)
plot(off2,PMCTrans2,'o--','Color',c(1,:))
hold on
plot(off2,PMCRefl2,'o--','Color',c(4,:))
plot(off2,PMCsum2,'o--','Color',c(5,:))
grid on
xlabel('PMC INOFFSET (mV)')
ylabel('Laser Power (W)')
legend('Transmission','Reflection','Sum')

subplot(122)
plot(off2,PMCEff2,'o--','Color',c(8,:))
grid on
xlabel('PMC INOFFSET (mV)')
ylabel('PMC Transmission Efficiency (%)')

%% data load
fil1 = load('PSD01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PSD02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('PSD03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PSD04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F1=[f4;f3;f2;f1];
M1=[m4;m3;m2;m1];
N1=(M1./1.99);

fil1 = load('PSD05.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PSD06.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('PSD07.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PSD08.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F2=[f4;f3;f2;f1];
M2=[m4;m3;m2;m1];
N2=(M2./1.99);

%% data load

fil1 = load('PSD09.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PSD10.asc', '-ascii'); 
f2 = fil2(5:801,1);
m2 = fil2(5:801,4);
fil3 = load('PSD11.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PSD12.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);

F3=[f2;f1];

F4=[f4;f3];
M4=[m4;m3];
M3=[m2;m1];
N3=(M3./1.99);
N4=(M4./1.99);


figure()
loglog(F1,N1,'Color',c(2,:))
hold on
loglog(F2,N2,'Color',c(3,:))
loglog(F4,N4,'Color',c(7,:))
loglog(F3,N3,'Color',c(6,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('Offset -3.2mV HEPA Fans On','Offset -3.8mV HEPA Fans On','Offset -3.2mV HEPA Fans Off','Offset -3.8mV HEPA Fans Off')
% legend('Laser Noise (9.5mW)','Laser Noise (4.3mW)','Laser Noise (2.2mW)','PD Dark Noise','Analyser Noise')
xlim([0,max(f1)])
% %ylim([-75,-30])

