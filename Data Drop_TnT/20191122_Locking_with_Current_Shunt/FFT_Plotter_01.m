% close all
% clear all
c=linspecer(9);
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD = 10.^(LISOPD.data(:,2)./20);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(10.^(gPD./20),pPD);

fil2 = load('OLG33DBI.asc', '-ascii'); 
ff2 = fil2(:,1);
gg2 = fil2(:,4);
pp2 = fil2(:,5);
OLTF2 = MP2complex(10.^(gg2./20),pp2);
CLTF2 = 1-1./(OLTF2+1);
CLG2 = 20.*log10(abs(CLTF2));
CLP2 = angle(CLTF2).*(180./pi);
%% data load
fil1 = load('ISSINN01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('ISSINN02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('ISSINN03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('ISSINN04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F00=[f4;f3;f2;f1];
M00=[m4;m3;m2;m1];
N00=(M00.*sqrt(2))./9.6; 

fil1 = load('DBBIN1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('DBBIN02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('DBBIN03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('DBBIN04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F01=[f4;f3;f2;f1];
M01=[m4;m3;m2;m1];
N01=(M01.*sqrt(2))./10.35; 

fil1 = load('DBB2IN01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('DBB2IN02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('DBB2IN03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('DBB2IN04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F02=[f4;f3;f2;f1];
M02=[m4;m3;m2;m1];
N02=(M02.*sqrt(2))./9.67; 


%% data load
fil1 = load('OL33N1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('OL33N2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('OL33N3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('IOL33N4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F2=[f4;f3;f2;f1];
M2=[m4;m3;m2;m1];
N2=(M2.*sqrt(2))./5.5;

fil1 = load('IL33N1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('IL33N2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('IL33N3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('IL33N4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F3=[f4;f3;f2;f1];
M3=[m4;m3;m2;m1];
N3=(M3.*sqrt(2))./5;

gPD0 = interp1(fPD,gPD,F3);
Nc0 = abs((interp1(F00,N00,ff2)./OLTF2).*CLTF2);
Nc1 = abs((interp1(F01,N01,ff2)./OLTF2).*CLTF2);
Nc2 = abs((interp1(F02,N02,ff2)./OLTF2).*CLTF2);
%%
figure()
loglog(F02,N02,'Color',c(2,:))
hold on
loglog(F01,N01,'Color',c(3,:))
loglog(F00,N00,'Color',c(1,:))
% loglog(F0,N0,'Color',c(4,:))
% loglog(f0,n0./40,'Color',c(5,:))
loglog(ff2,Nc0,':','Color',c(1,:))
loglog(ff2,Nc1,':','Color',c(2,:))
loglog(ff2,Nc2,':','Color',c(3,:))

% loglog(f2,n2,'Color',c(8,:))
% loglog(f1,n1,'Color',c(9,:))
loglog(F2,N2./138,'Color',c(6,:))
loglog(F3,(N3./gPD0)./138,'Color',c(7,:))
% legend('Amp II Output','Amp I Output','ISS PD','ISS PD with light pipe bellows')
grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
xlim([0,max(F0)])
% %ylim([-75,-30])

% G1 = 20.*log10(N0./N1);
G2 = 20.*log10(N0./N2);
% G3 = 20.*log10(N0./N3);
% G4 = 20.*log10(N0./N4);

% figure()
% % semilogx(F1,G1,'Color',c(1,:))
% semilogx(F2,G2,':','Color',c(1,:))
% hold on
% semilogx(F3,G3,'Color',c(3,:))
% % semilogx(F4,G4,':','Color',c(3,:))
% 
% 
% grid on
% xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
% ylabel({'Gain (dB)'},'Interpreter','latex')
% xlim([0,max(F0)])