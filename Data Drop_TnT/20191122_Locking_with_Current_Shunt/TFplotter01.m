close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));
%% Load CMod TF data AMPII
fil0 = load('OLG01.asc', '-ascii'); 
f0 = fil0(:,1);
g0 = fil0(:,4);
p0 = fil0(:,5);
OLTF0 = MP2complex(10.^(g0./20),p0);
CLTF0 = 1-1./(OLTF0+1);
CLG0 = 20.*log10(abs(CLTF0));
CLP0 = angle(CLTF0).*(180./pi);

fil1 = load('OLG33DB.asc', '-ascii'); 
f1 = fil1(:,1);
g1 = 20.*log10(fil1(:,4));
p1 = fil1(:,5);
OLTF1 = MP2complex(10.^(g1./20),p1);
CLTF1 = 1-1./(OLTF1+1);
CLG1 = 20.*log10(abs(CLTF1));
CLP1 = angle(CLTF1).*(180./pi);

fil2 = load('OLG33DBI.asc', '-ascii'); 
f2 = fil2(:,1);
g2 = fil2(:,4);
p2 = fil2(:,5);
OLTF2 = MP2complex(10.^(g2./20),p2);
CLTF2 = 1-1./(OLTF2+1);
CLG2 = 20.*log10(abs(CLTF2));
CLP2 = angle(CLTF2).*(180./pi);



%%
figure()
subplot(211)
semilogx(f1,g1,'Color',cc(1,:))
hold on
semilogx(f2,g2,'Color',cc(3,:))
semilogx(f0,g0,'Color',cc(7,:))
grid on
xlim([min(f1),max(f1)])
legend('Gain 33dB Integrators Off','Gain 33dB Integrators On','Gain 40dB Integrators Off')
title('Closed Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')

subplot(212)
semilogx(f1,p1,'Color',cc(1,:))
hold on
semilogx(f2,p2,'Color',cc(3,:))
semilogx(f0,p0,'Color',cc(7,:))
grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

figure()
subplot(211)
semilogx(f1,CLG1,'Color',cc(1,:))
hold on
semilogx(f2,CLG2,'Color',cc(3,:))
semilogx(f0,CLG0,'Color',cc(7,:))
grid on
xlim([min(f1),max(f1)])
legend('Gain 33dB Integrators Off','Gain 33dB Integrators On','Gain 40dB Integrators Off')
title('Closed Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')

subplot(212)
semilogx(f1,CLP1,'Color',cc(1,:))
hold on
semilogx(f2,CLP2,'Color',cc(3,:))
semilogx(f0,CLP0,'Color',cc(7,:))
grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

