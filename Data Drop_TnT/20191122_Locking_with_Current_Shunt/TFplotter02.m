close all 
clear all
cc=linspecer(9);
%%
fil0 = load('CS2005.asc', '-ascii'); 
f0 = fil0(:,1);
g0 = fil0(:,4);
p0 = fil0(:,5);
TF0 = MP2complex(10.^(g0./20),p0);

fil0 = load('CS201.asc', '-ascii'); 
f1 = fil0(:,1);
g1 = fil0(:,4);
p1 = fil0(:,5);
TF1 = MP2complex(10.^(g1./20),p1);

fil0 = load('CS202.asc', '-ascii'); 
f2 = fil0(:,1);
g2 = fil0(:,4);
p2 = fil0(:,5);
TF2 = MP2complex(10.^(g2./20),p2);

gWV1 = 10.^(g1./20).*((139)./10.36);
gWV2 = 10.^(g2./20).*((139)./10.35);
gWV0 = 10.^(g0./20).*((139.1)./10.28);

gV1 = 20.*log10((10.^(g1./20))./10.36);
gV2 = 20.*log10((10.^(g2./20))./10.35);
gV0 = 20.*log10((10.^(g0./20))./10.35);
%%
%500mV DBB
fil1 = importdata('A14051.txt'); 
ff0 = fil1(1:201,1);
gg0 = fil1(1:201,2);
pp0 = fil1(202:402,2);
%500mV ISS
fil1 = importdata('A14052.txt'); 
ff1 = fil1(1:201,1);
gg1 = (10.^(fil1(1:201,2)./20))./(10.60);
pp1 = fil1(202:402,2);
%1V DBB
fil1 = importdata('A14054.txt'); 
ff2 = fil1(1:201,1);
gg2 = fil1(1:201,2);
pp2 = fil1(202:402,2);
%1V ISS
fil1 = importdata('A14053.txt'); 
ff3 = fil1(1:201,1);
gg3 = (10.^(fil1(1:201,2)./20))./(9.88);
pp3 = fil1(202:402,2);
%2V DBB
fil1 = importdata('A14055.txt'); 
ff4 = fil1(1:201,1);
gg4 = fil1(1:201,2);
pp4 = fil1(202:402,2);
%2V ISS
fil1 = importdata('A14056.txt'); 
ff5 = fil1(1:201,1);
gg5 = (10.^(fil1(1:201,2)./20))./(9.3);
pp5 = fil1(202:402,2);

%%
figure()
subplot(211)
semilogx(f0,g0,'Color',cc(4,:))
hold on
semilogx(f1,g1,'Color',cc(5,:))
semilogx(f2,g2,'Color',cc(6,:))


grid on
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('500mV offset','1V offset','2V offset')
subplot(212)
semilogx(f0,p0,'Color',cc(4,:))
hold on
semilogx(f1,p1,'Color',cc(5,:))
semilogx(f2,p2,'Color',cc(6,:))


grid on
ylim([-90,90])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')


figure()
subplot(211)
semilogx(f1,gV0,'Color',cc(4,:))
hold on
semilogx(f2,gV1,'Color',cc(5,:))
semilogx(f2,gV2,'Color',cc(6,:))
semilogx(ff0,20.*log10(gg1),'Color',cc(1,:))
semilogx(ff1,20.*log10(gg3),'Color',cc(2,:))
semilogx(ff2,20.*log10(gg5),'Color',cc(3,:))

grid on
% ylim([-50,50])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain relative power (dB)')
leg = legend('0.5V','1V','2V');
title(leg,'Source 20,V, Offsets:')

subplot(212)
semilogx(f1,p0,'Color',cc(4,:))
hold on
semilogx(f2,p1,'Color',cc(5,:))
semilogx(f2,p2,'Color',cc(6,:))

semilogx(ff0,pp1,'Color',cc(1,:))
semilogx(ff1,pp3,'Color',cc(2,:))
semilogx(ff2,pp5,'Color',cc(3,:))


grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')