close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));



%% Load CMod TF data

fil2 = load('CSPDBAC1.asc', '-ascii'); 
f2 = fil2(:,1);
g2 = fil2(:,4);
p2 = fil2(:,5);
CSTF = MP2complex(10.^(g2./20),p2);

fil2 = load('AOMPDB02.asc', '-ascii'); 
f3 = fil2(:,1);
g3 = fil2(:,4);
p3 = fil2(:,5);
AOMTF=MP2complex(10.^(g3./20),p3);

fil2 = load('AOMPDB01.asc', '-ascii'); 
f4 = fil2(:,1);
g4 = fil2(:,4);
p4 = fil2(:,5);

fil2 = load('AOMOLG1.asc', '-ascii'); 
f5 = fil2(:,1);
g5 = fil2(:,4);
p5 = fil2(:,5);
LTF = MP2complex(10.^(g5./20),p5);
UGF = f5(find(g5<0,1,'first'));
UGP = p5(find(g5<0,1,'first'));
PM = 180+UGP;

%% Load ISS Board Mod TF
fil3 = load('ISSMDTF1.asc', '-ascii'); 
ff1 = fil3(:,1);
gg1 = fil3(:,4);
pp1 = fil3(:,5);

fil3 = load('ISSMDTF2.asc', '-ascii'); 
ff2 = fil3(:,1);
gg2 = fil3(:,4);
pp2 = unwrap(fil3(:,5))-180;

fil3 = load('ISSMDTF3.asc', '-ascii'); 
ff3 = fil3(:,1);
gg3 = fil3(:,4);
pp3 = fil3(:,5);

fil3 = load('ISSMDTF4.asc', '-ascii'); 
ff4 = fil3(:,1);
gg4 = fil3(:,4);
pp4 = fil3(:,5);

fil3 = load('ISSMDTF5.asc', '-ascii'); 
ff5 = fil3(:,1);
gg5 = fil3(:,4);
pp5 = unwrap(fil3(:,5))-180;





%% Plot results


figure()
subplot(211)
semilogx(f3,g3,'Color',cc(4,:))
hold on
semilogx(f2,g2,'Color',cc(3,:))
semilogx(f2,g3-g2,'--','Color',cc(5,:))

legend('AOM Response','Current Shunt Response','Difference')
grid on
% ylim([-50,50])
xlim([10,1e5])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')

subplot(212)
semilogx(f3,p3,'Color',cc(4,:))
hold on
semilogx(f2,p2,'Color',cc(3,:))
semilogx(f2,p3-p2,'--','Color',cc(5,:))

grid on
ylim([-180,180])
xlim([10,1e5])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

%%


figure()
subplot(211)
semilogx(f5,g5,'Color',cc(2,:))



grid on
% ylim([-50,50])
xlim([1e3,1e5])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('TnT ISS Open Loop')

subplot(212)
semilogx(f5,p5,'Color',cc(2,:))

grid on
ylim([-180,180])
xlim([1e3,1e5])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
% 
