close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));
%% Load 
fil1 = load('2811TF01.asc', '-ascii'); 
f1 = fil1(:,1);
g1 = fil1(:,4);
p1 = fil1(:,5);
OLTF1 = MP2complex(10.^(g1./20),p1);
CLTF1 = 1-1./(OLTF1+1);
CLG1 = 20.*log10(abs(CLTF1));
CLP1 = angle(CLTF1).*(180./pi);

fil1 = importdata('2811TF01.txt'); 
f11 = fil1.data(:,1);
g11 = fil1.data(:,2);
fil1 = importdata('2811TF01CH2.txt'); 
p11 = fil1.data(:,2);
OLTF11 = MP2complex(10.^(g11./20),p11);
CLTF11 = 1-1./(OLTF11+1);
CLG11 = 20.*log10(abs(CLTF11));
CLP11 = angle(CLTF11).*(180./pi);

fil2 = importdata('2811TF02.txt'); 
f12 = fil2.data(:,1);
g12 = fil2.data(:,2)-0.5;
fil1 = importdata('2811TF02CH2.txt'); 
p12 = fil1.data(:,2);
OLTF12 = MP2complex(10.^(g12./20),p12);
CLTF12 = 1-1./(OLTF12+1);
CLG12 = 20.*log10(abs(CLTF12));
CLP12 = angle(CLTF12).*(180./pi);

fil3 = importdata('2811TF03.txt'); 
f13 = fil3.data(:,1);
g13 = fil3.data(:,2);
fil1 = importdata('2811TF03CH2.txt'); 
p13 = fil1.data(:,2);
OLTF13 = MP2complex(10.^(g13./20),p13);
CLTF13 = 1-1./(OLTF13+1);
CLG13 = 20.*log10(abs(CLTF13));
CLP13 = angle(CLTF13).*(180./pi);

fil3 = importdata('2811TF20.asc'); 
f2 = fil3(:,1);
g2 = fil3(:,4);
p2 = fil3(:,5);
OLTF2 = MP2complex(10.^(g2./20),p2);
CLTF2 = 1-1./(OLTF2);
CLG2 = 20.*log10(abs(CLTF2));
CLP2 = angle(CLTF2).*(180./pi);

G2 = [g2;g12(f12>max(f2))];
F2 = [f2;f12(f12>max(f2))];
P2 = [p2;p12(f12>max(f2))];

UGP1 = f1(find(g1<0,1));
UGP2 = f2(find(g2<0,1));
% UGP3 = f13(find(g13<0,1));
PM1 = p1(find(g1<0,1));
PM2 = p2(find(g2<0,1));
% PM3 = p13(find(g13<0,1));

%%
figure()
subplot(211)
semilogx(f11,g11,'Color',cc(1,:))
hold on
semilogx(f12,g12,'Color',cc(2,:))
semilogx(f13,g13,'Color',cc(3,:))
% semilogx(F2,G2,'Color',cc(2,:))

semilogx([min(f1),max(f1)],[0,0],'k:')
% semilogx([UGP1,UGP1],[-10,60],':','Color',[cc(3,:),0.5])
% semilogx([UGP2,UGP2],[-10,60],':','Color',[cc(1,:),0.5])
% semilogx([UGP3,UGP3],[-10,60],':','Color',[cc(2,:),0.5])


grid on
xlim([min(F2),max(F2)])
ylim([-10,60])
title('Open Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
% title(leg,'Gain:')

subplot(212)
semilogx(f1,p1,'Color',cc(1,:))
hold on
% semilogx(f12,p12,'Color',cc(3,:))
semilogx(F2,P2,'Color',cc(2,:))

% semilogx([UGP1,UGP1],[-180,180],':','Color',[cc(3,:),0.5])
% semilogx([UGP2,UGP2],[-180,180],':','Color',[cc(1,:),0.5])
% semilogx([UGP3,UGP3],[-180,180],':','Color',[cc(2,:),0.5])

% semilogx([min(f1),max(f1)],[PM1,PM1],':','Color',[cc(3,:),0.5])
% semilogx([min(f1),max(f1)],[PM2,PM2],':','Color',[cc(1,:),0.5])
% semilogx([min(f11),max(f11)],[PM3,PM3],':','Color',[cc(2,:),0.5])


grid on
ylim([-180,180])
xlim([min(F2),max(F2)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
% 
% figure()
% subplot(211)
% semilogx(f1,CLG1,'Color',cc(1,:))
% hold on
% semilogx(f2,CLG2,'Color',cc(2,:))
% % semilogx(f13,CLG11,'Color',cc(3,:))
% % semilogx(f1,CLG1,'Color',cc(4,:))
% 
% 
% grid on
% xlim([min(f1),max(f1)])
% title('Closed Loop TF')
% xlabel('Frequency (Hz)')
% ylabel('Gain (dB)')
% leg = legend('20dB','24dB','25dB');
% title(leg,'Gain')
% 
% subplot(212)
% semilogx(f1,CLP1,'Color',cc(1,:))
% hold on
% semilogx(f2,CLP2,'Color',cc(2,:))
% % semilogx(f13,CLP11,'Color',cc(3,:))
% % semilogx(f1,CLP1,'Color',cc(4,:))
% 
% grid on
% ylim([-180,180])
% xlim([min(f1),max(f1)])
% xlabel('Frequency (Hz)')
% ylabel('Phase (deg)')