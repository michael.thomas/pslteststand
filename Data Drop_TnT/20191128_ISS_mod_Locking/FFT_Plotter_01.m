close all
clear all
c=linspecer(9);
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD0 = 10.^(LISOPD.data(:,2)./20);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(gPD0,pPD);
%% data load

fil1 = load('2811NM07.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil1 = load('2811NM05.asc', '-ascii');
f2 = fil1(102:801,1);
m2 = fil1(102:801,2);
fil3 = load('2811NM03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('2811NM01.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F0=[f4;f3;f2;f1];
n0=([m4;m3;m2;m1])./11.9;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
% gPD = interp1(fPD,gPD,F0);
N0=n0;

fil1 = load('2811NM08.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil1 = load('2811NM06.asc', '-ascii');
f2 = fil1(102:801,1);
m2 = fil1(102:801,2);
fil3 = load('2811NM04.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('2811NM02.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F1=[f4;f3;f2;f1];
n1=([m4;m3;m2;m1])./11.9;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
gPD = interp1(fPD,gPD0,F1);
N1=n1./gPD;

fil1 = load('2811NM09.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('2811NM11.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('2811NM13.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('2811NM15.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F2=[f4;f3;f2;f1];
n2=([m4;m3;m2;m1])./10.7;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
% gPD = interp1(fPD,gPD,F0);
N2=n2;
% 
fil1 = load('2811NM10.asc', '-ascii');
f11 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('2811NM12.asc', '-ascii'); 
f22 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('2811NM14.asc', '-ascii'); 
f33 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('2811NM16.asc', '-ascii'); 
f44 = fil4(:,1);
m4 = fil4(:,2);
F3=[f4;f3;f2;f1];
n3=([m4;m3;m2;m1])./10.7;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
gPD = interp1(fPD,gPD0,F3);
N3=n3./gPD;
% 

% 


% 
% 
figure()
loglog(F0,N0,'Color',c(1,:))
hold on
loglog(F1,N1,'Color',c(2,:))
loglog(F2,N2,'Color',c(3,:))
loglog(F3,N3,'Color',c(4,:))


grid on
% legend('25dB','20dB','15dB');
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
xlim([0,max(F2)])
% %ylim([-75,-30])