function [a,b,c,d] = integrator(hf,Gain)


z	= -hf;
p	= -0;
k	= Gain;

[a,b,c,d] = zp2ss(z,p,k);