close all
clear all
%%
fil1 = load('INT1OFF.asc', '-ascii'); 
f1 = fil1(:,1);
m1 = fil1(:,4);
p1 = fil1(:,5);

fil2 = load('INT1ON.asc', '-ascii'); 
f2 = fil2(:,1);
m2 = fil2(:,4);
p2 = fil2(:,5);

fil3 = load('INT102.asc', '-ascii'); 
f3 = fil3(:,1);
m3 = fil3(:,4);
p3 = fil3(:,5);

fil4 = load('R109.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
p4 = fil4(:,5);

fil5 = load('X28OFF0.asc', '-ascii'); 
f5 = fil5(:,1);
m5 = fil5(:,4);
p5 = fil5(:,5);
%%
[a,b,c,d] = transint(800,4.3e3,1);
sys=ss(a,b,c,d);
[gfil,pfil] = bode(sys,f1);
[a,b,c,d] = integrator(4.3e3,0.08);
sys=ss(a,b,c,d);
[gfil2,pfil2] = bode(sys,f1);


%% Plot
figure()
subplot(211)
semilogx(f1,m1,'r','LineWidth',3)
hold on
% semilogx(f2,m2,'b--')
semilogx(f3,m3,'b','LineWidth',3)
semilogx(f1,20.*log10(gfil(:)),'k--')
semilogx(f1,20.*log10(gfil2(:)),'k:')
% semilogx(f4,m4)
% semilogx(f5,m5)
grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('Integrators OFF','Integrators ON','Fit trans-int fp=800Hz, fz=4.3kHz','fit Integrator fz=4.3kHz')
xlim([min(f1),max(f1)])
ylim([-25,10])
subplot(212)
semilogx(f1,p1,'r','LineWidth',3)
hold on
semilogx(f3,p3,'b','LineWidth',3)
semilogx(f1,pfil(:),'k--')
semilogx(f1,pfil2(:),'k:')

grid on
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
ylim([-180,180])
xlim([min(f1),max(f1)])
%%
fil0 = load('ISSIO.asc', '-ascii'); 
f0 = fil0(:,1);
m0 = fil0(:,4);
p0 = fil0(:,5);

fil1 = load('ISS2AOM1.asc', '-ascii'); 
f1 = fil1(:,1);
m1 = fil1(:,4);
p1 = fil1(:,5);

fil2 = load('ISS2AOM2.asc', '-ascii'); 
f2 = fil2(:,1);
m2 = fil2(:,4);
p2 = fil2(:,5);

%% Plot
figure()
subplot(211)
semilogx(f1,m1,'m')
hold on
semilogx(f2,m2,'g')
semilogx(f0,m0,'k:')
legend('ISS -> ISS PDA','ISS -> DBB RPN PD','ISS Servo')
% hold off
grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
xlim([min(f0),max(f0)])
ylim([-50,30])
%legend('EOM Electronic Gain','PZT Electronics Gain')

subplot(212)
semilogx(f1,p1,'m')
hold on
semilogx(f2,p2,'g')
semilogx(f0,p0,'k:')

grid on
% legend('Trans-Int','+BiQuad 1','+BiQuad 2','+BiQuad 3','+BiQuad 4')
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
ylim([-180,0])
xlim([min(f0),max(f0)])

%% AOM resposne
AOMfil = importdata('AOMTF.out');
fAOM = AOMfil.data(:,1);
gAOM = 10.^(AOMfil.data(:,2)./20);
pAOM = AOMfil.data(:,3);
AOMTF = MP2complex(10.^(gAOM./20),pAOM);

ServoTF = MP2complex(10.^(m0./20),p0);
TF1 = MP2complex(10.^(m1./20),p1);
TF2 = MP2complex(10.^(m2./20),p2);

AOM1 = TF1./ServoTF;
AOM2 = TF2./ServoTF;

AOMm1 = abs(AOM1).*(90./10.6);
AOMm2 = abs(AOM2).*(90./2.45);
AOMp1 = angle(AOM1).*(180./pi);
AOMp2 = angle(AOM2).*(180./pi);

[a1,b1,c1,d1] = lowpass(1.6e5,77);
sysA=ss(a1,b1,c1,d1);
[a1,b1,c1,d1] = lowpass(2e5,1);
sysB=ss(a1,b1,c1,d1);
sysAOM = series(sysA,sysB);
[gA,pA] = bode(sysAOM,fAOM);
gA=gA(:);
pA=pA(:);

AOMfit = [fAOM,20.*log10(gA),pA];


figure()
subplot(211)
loglog(f1,AOMm1,'m')
hold on
loglog(f2,AOMm2,'g')
% semilogx(fAOM,gAOM,'b')
semilogx(fAOM,gA,'b')

legend('AOM -> ISS PDA','AOM -> DBB RPN PD')
% hold off
grid on
xlabel('Frequency (Hz)')
ylabel('Gain (W/V)')
xlim([min(f0),max(f0)])
% ylim([0,4])
%legend('EOM Electronic Gain','PZT Electronics Gain')

subplot(212)
semilogx(f1,AOMp1,'m')
hold on
semilogx(f2,AOMp2,'g')
% semilogx(fAOM,pAOM,'b')
semilogx(fAOM,pA,'b')

grid on
% legend('Trans-Int','+BiQuad 1','+BiQuad 2','+BiQuad 3','+BiQuad 4')
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
ylim([-90,10])
xlim([min(f0),max(f0)])

AOMresp1 = [f1,AOMm1,AOMp1];