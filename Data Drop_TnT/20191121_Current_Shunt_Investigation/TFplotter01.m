close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));
%% Load CMod TF data AMPII
fil2 = load('AMPIICS1.asc', '-ascii'); 
f0 = fil2(:,1);
g0 = 20.*log10(fil2(:,4));
p0 = fil2(:,5);

fil2 = load('AMPIICS2.asc', '-ascii'); 
f00 = fil2(:,1);
g00 = fil2(:,4);
p00 = fil2(:,5);

%% Load CMod TF data
fil2 = load('OFF1V.asc', '-ascii'); 
f1 = fil2(:,1);
g1 = fil2(:,4);
p1 = fil2(:,5);

fil2 = load('OFF800.asc', '-ascii'); 
f2 = fil2(:,1);
g2 = fil2(:,4);
p2 = fil2(:,5);
% CSTF2 = MP2complex(10.^(g2./20),p2);

fil2 = load('OFF400.asc', '-ascii'); 
f3 = fil2(:,1);
g3 = fil2(:,4);
p3 = fil2(:,5);
% CSTF3=MP2complex(10.^(g3./20),p3);
fil2 = load('OFF400DB.asc', '-ascii'); 
f3b = fil2(:,1);
g3b = fil2(:,4);
p3b = fil2(:,5);

fil2 = load('OFF600.asc', '-ascii'); 
f4 = fil2(:,1);
g4 = fil2(:,4);
p4 = fil2(:,5);
% CSTF4=MP2complex(10.^(g4./20),p4);

fil2 = load('OFFN400.asc', '-ascii'); 
f5 = fil2(:,1);
g5 = fil2(:,4);
p5 = fil2(:,5);

fil2 = load('OFF0.asc', '-ascii'); 
f6 = fil2(:,1);
g6 = fil2(:,4);
p6 = fil2(:,5);

%% CONVERTING Current Shunt Response measured in (V/V) to (W/V)
gWV1 = 10.^(g1./20).*((112.2+9.4)./10.36);
gWV2 = 10.^(g2./20).*((112.4+10.3)./10.35);
gWV3 = 10.^(g3./20).*((139.1)./10.28);
gWV3b = 10.^(g3b./20).*((139.1)./9.64);
gWV4 = 10.^(g4./20).*((111.6+15.5)./10.17);
gWV0 = 10.^(g0./20).*((139.1)./10.29);
gWV00 = 10.^(g00./20).*((65.9)./10.01);
%% CONVERTING Current Shunt Response to relative power (W/W)/V
gV1 = 20.*log10((10.^(g1./20))./10.36);
gV2 = 20.*log10((10.^(g2./20))./10.35);
gV3 = 20.*log10((10.^(g3./20))./10.28);
gV3b = 20.*log10((10.^(g3b./20))./9.64);
gV4 = 20.*log10((10.^(g4./20))./10.17);
gV0 = 20.*log10((10.^(g0./20))./10.29);
gV00 = 20.*log10((10.^(g00./20).*(65.9./10.01))./139.1);

CSresp2 = [f1,gWV1,p1];
%%
figure()
subplot(211)
semilogx(f1,g1,'Color',cc(1,:))
hold on
semilogx(f2,g2,'Color',cc(2,:))
semilogx(f4,g4,'Color',cc(4,:))
semilogx(f3,g3,'Color',cc(3,:))
% semilogx(f5,g5,'Color',cc(5,:))
% semilogx(f6,g6,'Color',cc(6,:))
semilogx(f3b,g3b,':','Color',cc(3,:))
semilogx(f0,g0,'Color',cc(7,:))
semilogx(f00,g00,'Color',cc(9,:))

grid on
% ylim([-50,50])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('1V','0.8V','0.6V','0.4V')
subplot(212)
semilogx(f1,p1,'Color',cc(1,:))
hold on
semilogx(f2,p2,'Color',cc(2,:))
semilogx(f4,p4,'Color',cc(4,:))
semilogx(f3,p3,'Color',cc(3,:))
% semilogx(f5,p5,'Color',cc(5,:))
% semilogx(f6,p6,'Color',cc(6,:))
semilogx(f3b,p3b,':','Color',cc(3,:))
semilogx(f0,p0,'Color',cc(7,:))
semilogx(f00,p00,'Color',cc(9,:))

grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
figure()
subplot(211)
% semilogx(f1,gV1,'Color',cc(1,:))
% hold on
% loglog(f2,gWV2,'Color',cc(2,:))
% loglog(f4,gWV4,'Color',cc(4,:))
loglog(f3,gWV3,'Color',cc(3,:))
hold on
loglog(f3b,gWV3b,'Color',cc(5,:))
loglog(f0,gWV0,'Color',cc(7,:))
loglog(f00,gWV00,'Color',cc(9,:))

grid on
% ylim([-50,50])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain (W/V)')
% legend('1V','0.8V','0.6V','0.4V','0.4V(DBB)')
legend('AMP I (ISS)','AMP I (DBB)','AMP II (ISS)','AMP II (DBB)')
subplot(212)
% semilogx(f1,p1,'Color',cc(1,:))
% hold on
% semilogx(f2,p2,'Color',cc(2,:))
% semilogx(f4,p4,'Color',cc(4,:))
semilogx(f3,p3,'Color',cc(3,:))
hold on
semilogx(f3b,p3b,'Color',cc(5,:))
semilogx(f0,p0,'Color',cc(7,:))
semilogx(f00,p00,'Color',cc(9,:))

grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure()
subplot(211)
semilogx(f1,gV1,'Color',cc(1,:))
hold on
semilogx(f2,gV2,'Color',cc(2,:))
semilogx(f4,gV4,'Color',cc(4,:))
semilogx(f3,gV3,'Color',cc(3,:))
semilogx(f3b,gV3b,'Color',[0.7,0.7,0])
semilogx(f0,gV0,'Color',cc(7,:))
semilogx(f00,gV00,'Color',cc(9,:))

grid on
% ylim([-50,50])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain relative power (dB)')
leg = legend('1V','0.8V','0.6V','0.4V');
title(leg,'Offsets:')

subplot(212)
semilogx(f0,p0,'Color',cc(7,:))
hold on
semilogx(f00,p00,'Color',cc(9,:))
semilogx(f3,p3,'Color',cc(3,:))
semilogx(f3b,p3b,'Color',[0.7,0.7,0])
semilogx(f1,p1,'Color',cc(1,:))
semilogx(f2,p2,'Color',cc(2,:))
semilogx(f4,p4,'Color',cc(4,:))

leg1 = legend('AMP II (ISS)','AMP II (DBB)','AMP I (ISS)','AMP I (DBB)');
title(leg1,'Offset 0.4V')

grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')