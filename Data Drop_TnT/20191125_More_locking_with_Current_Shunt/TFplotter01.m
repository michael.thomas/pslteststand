close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));
%% Load CMod TF data AMPII

fil1 = load('CSTF101.asc', '-ascii'); 
f11 = fil1(:,1);
g11 = fil1(:,4);
p11 = fil1(:,5);
OLTF11 = MP2complex(10.^(g11./20),p11);
CLTF11 = 1-1./(OLTF11+1);
CLG11 = 20.*log10(abs(CLTF11));
CLP11 = angle(CLTF11).*(180./pi);

fil2 = load('CSTF102.asc', '-ascii'); 
f12 = fil2(:,1);
g12 = fil2(:,4);
p12 = fil2(:,5);
OLTF12 = MP2complex(10.^(g12./20),p12);
CLTF12 = 1-1./(OLTF12+1);
CLG12 = 20.*log10(abs(CLTF12));
CLP12 = angle(CLTF12).*(180./pi);

fil2 = load('CSTF103.asc', '-ascii'); 
f13 = fil2(:,1);
g13 = fil2(:,4);
p13 = fil2(:,5);
OLTF13 = MP2complex(10.^(g13./20),p13);
CLTF13 = 1-1./(OLTF13+1);
CLG13 = 20.*log10(abs(CLTF13));
CLP13 = angle(CLTF13).*(180./pi);

fil2 = load('CSTF104.asc', '-ascii'); 
f14 = fil2(:,1);
g14 = fil2(:,4);
p14 = fil2(:,5);
OLTF14 = MP2complex(10.^(g14./20),p14);
CLTF14 = 1-1./(OLTF14+1);
CLG14 = 20.*log10(abs(CLTF14));
CLP14 = angle(CLTF14).*(180./pi);

fil2 = load('CSTF105.asc', '-ascii'); 
f15 = fil2(:,1);
g15 = fil2(:,4);
p15 = fil2(:,5);
OLTF15 = MP2complex(10.^(g15./20),p15);
CLTF15 = 1-1./(OLTF15+1);
CLG15 = 20.*log10(abs(CLTF15));
CLP15 = angle(CLTF15).*(180./pi);

fil2 = load('CSTF106.asc', '-ascii'); 
f16 = fil2(:,1);
g16 = fil2(:,4);
p16 = fil2(:,5);
OLTF16 = MP2complex(10.^(g16./20),p16);
CLTF16 = 1-1./(OLTF16+1);
CLG16 = 20.*log10(abs(CLTF16));
CLP16 = angle(CLTF16).*(180./pi);

fil1 = load('CSTF401.asc', '-ascii'); 
f41 = fil1(:,1);
g41 = fil1(:,4);
p41 = fil1(:,5);
OLTF41 = MP2complex(10.^(g41./20),p41);
CLTF41 = 1-1./(OLTF41+1);
CLG41 = 20.*log10(abs(CLTF41));
CLP41 = angle(CLTF41).*(180./pi);

fil2 = load('CSTF402.asc', '-ascii'); 
f42 = fil2(:,1);
g42 = fil2(:,4);
p42 = fil2(:,5);
OLTF42 = MP2complex(10.^(g42./20),p42);
CLTF42 = 1-1./(OLTF42+1);
CLG42 = 20.*log10(abs(CLTF42));
CLP42 = angle(CLTF42).*(180./pi);

fil2 = load('CSTF403.asc', '-ascii'); 
f43 = fil2(:,1);
g43 = fil2(:,4);
p43 = fil2(:,5);
OLTF43 = MP2complex(10.^(g43./20),p43);
CLTF43 = 1-1./(OLTF43+1);
CLG43 = 20.*log10(abs(CLTF43));
CLP43 = angle(CLTF43).*(180./pi);

fil2 = load('CSTF404.asc', '-ascii'); 
f44 = fil2(:,1);
g44 = fil2(:,4);
p44 = fil2(:,5);
OLTF44 = MP2complex(10.^(g44./20),p44);
CLTF44 = 1-1./(OLTF44+1);
CLG44 = 20.*log10(abs(CLTF44));
CLP44 = angle(CLTF44).*(180./pi);

fil2 = load('CSTF405.asc', '-ascii'); 
f45 = fil2(:,1);
g45 = fil2(:,4);
p45 = fil2(:,5);
OLTF45 = MP2complex(10.^(g45./20),p45);
CLTF45 = 1-1./(OLTF45+1);
CLG45 = 20.*log10(abs(CLTF45));
CLP45 = angle(CLTF45).*(180./pi);

fil2 = load('CSTF406.asc', '-ascii'); 
f46 = fil2(:,1);
g46 = fil2(:,4);
p46 = fil2(:,5);
OLTF46 = MP2complex(10.^(g46./20),p46);
CLTF46 = 1-1./(OLTF46+1);
CLG46 = 20.*log10(abs(CLTF46));
CLP46 = angle(CLTF46).*(180./pi);

fil1 = load('CSTF201.asc', '-ascii'); 
f21 = fil1(:,1);
g21 = fil1(:,4);
p21 = fil1(:,5);
OLTF21 = MP2complex(10.^(g21./20),p21);
CLTF21 = 1-1./(OLTF21+1);
CLG21 = 20.*log10(abs(CLTF21));
CLP21 = angle(CLTF21).*(180./pi);

fil2 = load('CSTF202.asc', '-ascii'); 
f22 = fil2(:,1);
g22 = fil2(:,4);
p22 = fil2(:,5);
OLTF22 = MP2complex(10.^(g22./20),p22);
CLTF22 = 1-1./(OLTF22+1);
CLG22 = 20.*log10(abs(CLTF22));
CLP22 = angle(CLTF22).*(180./pi);

fil1 = load('CSTF301.asc', '-ascii'); 
f31 = fil1(:,1);
g31 = fil1(:,4);
p31 = fil1(:,5);
OLTF31 = MP2complex(10.^(g31./20),p31);
CLTF31 = 1-1./(OLTF31+1);
CLG31 = 20.*log10(abs(CLTF31));
CLP31 = angle(CLTF31).*(180./pi);


%%
figure()
subplot(211)
% semilogx(f12,g12,':','Color',cc(2,:))
% semilogx(f11,g11,':','Color',cc(1,:))
% hold on
% % semilogx(f13,g13,':','Color',cc(3,:))
% % semilogx(f14,g14,':','Color',cc(4,:))
% semilogx(f15,g15,':','Color',cc(5,:))
% semilogx(f16,g16,':','Color',cc(6,:))

semilogx(f42,g42,'Color',cc(2,:))
hold on
semilogx(f43,g43,'Color',cc(3,:))
semilogx(f41,g41,'Color',cc(1,:))
semilogx(f44,g44,'Color',cc(4,:))
semilogx(f45,g45,'Color',cc(5,:))
semilogx(f46,g46,'Color',cc(6,:))


grid on
xlim([min(f11),max(f11)])
title('Open Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('21.2 dB','26.1dB','29.5dB','35.1dB','40dB');
% leg = legend('V(PD)=5V, 29.5dB','V(PD)=5V, 35.1dB','V(PD)=10V, 29.5dB','V(PD)=10V, 35.1dB');
title(leg,'Gain:')

subplot(212)
% semilogx(f12,p12,':','Color',cc(2,:))
% semilogx(f11,p11,':','Color',cc(1,:))
% hold on
% semilogx(f13,p13,'Color',cc(3,:))
% semilogx(f14,p14,'Color',cc(4,:))
% semilogx(f15,p15,':','Color',cc(5,:))
% semilogx(f16,p16,':','Color',cc(6,:))

semilogx(f42,p42,'Color',cc(2,:))
hold on
semilogx(f43,p43,'Color',cc(3,:))
semilogx(f41,p41,'Color',cc(1,:))
semilogx(f44,p44,'Color',cc(4,:))
semilogx(f45,p45,'Color',cc(5,:))
semilogx(f46,p46,'Color',cc(5,:))

grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

figure()
subplot(211)
% semilogx(f12,CLG12,':','Color',cc(2,:))
semilogx(f11,CLG11,':','Color',cc(1,:))
hold on
% semilogx(f13,CLG13,':','Color',cc(3,:))
% semilogx(f14,CLG14,':','Color',cc(4,:))
semilogx(f15,CLG15,':','Color',cc(5,:))
% semilogx(f16,CLG16,':','Color',cc(6,:))

% semilogx(f12,CLG42,'Color',cc(2,:))
% hold on
% semilogx(f13,CLG43,'Color',cc(3,:))
semilogx(f11,CLG41,'Color',cc(1,:))
% semilogx(f14,CLG44,'Color',cc(4,:))
semilogx(f15,CLG45,'Color',cc(5,:))
% semilogx(f16,CLG46,'Color',cc(5,:))

grid on
xlim([min(f11),max(f11)])
title('Closed Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('V(PD)=5V, 29.5dB','V(PD)=5V, 35.1dB','V(PD)=10V, 29.5dB','V(PD)=10V, 35.1dB');
title(leg,'Gain:')

subplot(212)
% semilogx(f12,CLP12,':','Color',cc(2,:))
semilogx(f11,CLP11,':','Color',cc(1,:))
hold on
% semilogx(f13,CLP13,':','Color',cc(3,:))
% semilogx(f14,CLP14,':','Color',cc(4,:))
semilogx(f15,CLP15,':','Color',cc(5,:))
% semilogx(f16,CLP16,':','Color',cc(6,:))

% semilogx(f12,CLP42,'Color',cc(2,:))
% hold on
% semilogx(f13,CLP43,'Color',cc(3,:))
semilogx(f11,CLP41,'Color',cc(1,:))
% semilogx(f14,CLP44,'Color',cc(4,:))
semilogx(f15,CLP45,'Color',cc(5,:))
% semilogx(f16,CLP46,'Color',cc(5,:))
grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

figure()
subplot(211)
semilogx(f15,g15,'Color',cc(5,:))
hold on
semilogx(f21,g21,'Color',cc(7,:))
semilogx(f22,g22,'Color',cc(8,:))
grid on
xlim([min(f11),max(f11)])
title('Open Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('10','5','0');
title(leg,'Offset')

subplot(212)
semilogx(f15,p15,'Color',cc(5,:))
hold on
semilogx(f21,p21,'Color',cc(7,:))
semilogx(f22,p22,'Color',cc(8,:))
grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

figure()
subplot(211)
semilogx(f15,CLG15,'Color',cc(5,:))
hold on
semilogx(f21,CLG21,'Color',cc(7,:))
semilogx(f22,CLG22,'Color',cc(8,:))
grid on
xlim([min(f11),max(f11)])
title('Closed Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('10','5','0');
title(leg,'Offset')

subplot(212)
semilogx(f15,CLP15,'Color',cc(5,:))
hold on
semilogx(f21,CLP21,'Color',cc(7,:))
semilogx(f22,CLP22,'Color',cc(8,:))
grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure()
subplot(211)
semilogx(f31,g31,'Color',cc(1,:))
hold on
semilogx(f22,g22,'Color',cc(2,:))

grid on
xlim([min(f11),max(f11)])
title('Open Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
title(leg,'Offset')

subplot(212)
semilogx(f31,p31,'Color',cc(1,:))
hold on
semilogx(f22,p22,'Color',cc(2,:))

grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Gain = [21.2,26.1,29.5,35.1,40];
UGP = [13.2,20.6,27.2,32.0,22.1];
PM = [45.3,45.0,38.8,23.4,12.9];


figure()
yyaxis left
plot(Gain,UGP,'*')
ylabel('Unity Gain Frequency (kHz)')
yyaxis right
plot(Gain,PM,'o')
xlabel('Gain (dB)')
ylabel('Phase Margin (deg)')