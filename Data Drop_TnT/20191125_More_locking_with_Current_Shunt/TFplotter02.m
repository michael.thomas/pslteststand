close all 
clear all
cc=linspecer(9);
%%
fil0 = load('CS2005.asc', '-ascii'); 
f0 = fil0(:,1);
g0 = fil0(:,4);
p0 = fil0(:,5);
TF0 = MP2complex(10.^(g0./20),p0);

fil0 = load('CS201.asc', '-ascii'); 
f1 = fil0(:,1);
g1 = fil0(:,4);
p1 = fil0(:,5);
TF1 = MP2complex(10.^(g1./20),p1);

fil0 = load('CS202.asc', '-ascii'); 
f2 = fil0(:,1);
g2 = fil0(:,4);
p2 = fil0(:,5);
TF2 = MP2complex(10.^(g2./20),p2);

gWV1 = 10.^(g1./20).*((139)./10.36);
gWV2 = 10.^(g2./20).*((139)./10.35);
gWV0 = 10.^(g0./20).*((139.1)./10.28);

gV1 = 20.*log10((10.^(g1./20))./10.36);
gV2 = 20.*log10((10.^(g2./20))./10.35);
gV0 = 20.*log10((10.^(g0./20))./10.35);

%%
figure()
subplot(211)
semilogx(f0,g0,'Color',cc(4,:))
hold on
semilogx(f1,g1,'Color',cc(5,:))
semilogx(f2,g2,'Color',cc(6,:))

grid on
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('500mV offset','1V offset','2V offset')
subplot(212)
semilogx(f0,p0,'Color',cc(4,:))
hold on
semilogx(f1,p1,'Color',cc(5,:))
semilogx(f2,p2,'Color',cc(6,:))
grid on
ylim([-90,90])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')


figure()
subplot(211)
semilogx(f1,gV0,'Color',cc(4,:))
hold on
semilogx(f2,gV1,'Color',cc(5,:))
semilogx(f2,gV2,'Color',cc(6,:))


grid on
% ylim([-50,50])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Gain relative power (dB)')
leg = legend('0.5V','1V','2V');
title(leg,'Source 20,V, Offsets:')

subplot(212)
semilogx(f1,p0,'Color',cc(4,:))
hold on
semilogx(f2,p1,'Color',cc(5,:))
semilogx(f2,p2,'Color',cc(6,:))


% leg1 = legend('AMP II (ISS)','AMP II (DBB)','AMP I (ISS)','AMP I (DBB)');
% title(leg1,'Offset 0.4V')

grid on
ylim([-180,180])
xlim([min(f1),max(f1)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')