% close all
% clear all
c=linspecer(9);
%% TF 
fil2 = load('CSTF403.asc', '-ascii'); 
f43 = fil2(:,1);
g43 = fil2(:,4);
p43 = fil2(:,5);
OLTF43 = MP2complex(10.^(g43./20),p43);
CLTF43 = 1-1./(OLTF43+1);
CLG43 = 20.*log10(abs(CLTF43));
CLP43 = angle(CLTF43).*(180./pi);

fil2 = load('CSTF406.asc', '-ascii'); 
f46 = fil2(:,1);
g46 = fil2(:,4);
p46 = fil2(:,5);
OLTF46 = MP2complex(10.^(g46./20),p46);
CLTF46 = 1-1./(OLTF46+1);
CLG46 = 20.*log10(abs(CLTF46));
CLP46 = angle(CLTF46).*(180./pi);

fil2 = load('CSTF404.asc', '-ascii'); 
f44 = fil2(:,1);
g44 = fil2(:,4);
p44 = fil2(:,5);
OLTF44 = MP2complex(10.^(g44./20),p44);
CLTF44 = 1-1./(OLTF44+1);
CLG44 = 20.*log10(abs(CLTF44));
CLP44 = angle(CLTF44).*(180./pi);

fil2 = load('CSTF405.asc', '-ascii'); 
f45 = fil2(:,1);
g45 = fil2(:,4);
p45 = fil2(:,5);
OLTF45 = MP2complex(10.^(g45./20),p45);
CLTF45 = 1-1./(OLTF45+1);
CLG45 = 20.*log10(abs(CLTF45));
CLP45 = angle(CLTF45).*(180./pi);

%% data load

fil1 = load('FRFFT01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('FRFFT02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('FRFFT03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('FRFFT04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F0=[f4;f3;f2;f1];
n0=([m4;m3;m2;m1].*sqrt(2))./10;
% nO30=([mO4;mO3;mO2;mO1])./11.25;


fil1 = load('CSFFT01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
mO1 = fil1(102:801,4);
fil2 = load('CSFFT02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
mO2 = fil2(102:801,4);
fil3 = load('CSFFT03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
mO3 = fil3(102:801,4);
fil4 = load('CSFFT04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
mO4 = fil4(:,4);
F1=[f4;f3;f2;f1];
% nI30=[m4;m3;m2;m1];
nI30=([mO4;mO3;mO2;mO1].*sqrt(2))./139;
nO30=abs((nI30)./interp1(f44,CLTF44,F1).*interp1(f44,OLTF44,F1)); 

fil1 = load('CSFFT11.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
mO1 = fil1(102:801,4);
fil2 = load('CSFFT12.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
mO2 = fil2(102:801,4);
fil3 = load('CSFFT13.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
mO3 = fil3(102:801,4);
fil4 = load('CSFFT14.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
mO4 = fil4(:,4);
F2=[f4;f3;f2;f1];
% nI35=[m4;m3;m2;m1];
nI35=([mO4;mO3;mO2;mO1].*sqrt(2))./138; 
nO35=abs((nI35)./interp1(f45,CLTF45,F2).*interp1(f45,OLTF45,F2)); 

fil1 = load('CSFFT21.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
mO1 = fil1(102:801,4);
fil2 = load('CSFFT22.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
mO2 = fil2(102:801,4);
fil3 = load('CSFFT23.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
mO3 = fil3(102:801,4);
fil4 = load('CSFFT24.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
mO4 = fil4(:,4);
F3=[f4;f3;f2;f1];
nI25=([m4;m3;m2;m1].*sqrt(2))./138;
nO25=abs((nI25).*interp1(f43,OLTF43,F3)); 
% ./interp1(f43,CLTF43,F3)
fil1 = load('CSFFT41.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
mO1 = fil1(102:801,4);
fil2 = load('CSFFT42.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
mO2 = fil2(102:801,4);
fil3 = load('CSFFT43.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
mO3 = fil3(102:801,4);
fil4 = load('CSFFT44.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
mO4 = fil4(:,4);
F3=[f4;f3;f2;f1];
nI40=([m4;m3;m2;m1].*sqrt(2))./138;
% nO40=([m4;m3;m2;m1]./11.3); 
nO40=abs((nI40)./interp1(f46,CLTF46,F3).*interp1(f46,OLTF46,F3)); 

%% Loading in ISS PD FILT reesponse to correct error point noise measurements
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD = 10.^(LISOPD.data(:,2)./20);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(gPD,pPD);
gPD = interp1(fPD,gPD,F1);
%%
figure()
loglog(F1,(nI25)./(gPD),'Color',c(2,:))
hold on
% loglog(F1,(nO25)./(gPD),'--','Color',c(2,:))
loglog(F1,(nI30./2.05)./gPD,'Color',c(3,:))
% loglog(F1,(nO30.*2.05)./gPD,':','Color',c(3,:))
loglog(F1,(nI35./4.5)./gPD,'Color',c(1,:))
% loglog(F1,(nO35.*0.8)./gPD,':','Color',c(1,:))
% loglog(F1,(nI40./6)./gPD,'Color',c(4,:))
% loglog(F1,nO40./gPD,':','Color',c(4,:))
% % loglog(F1,nO30,':','Color',c(3,:))
% % loglog(F1,nO35,':','Color',c(1,:))
loglog(F0,n0./gPD,'Color',c(5,:))
% % loglog(F0,gPD,'Color',c(2,:))

% loglog(F2,N2,'Color',c(4,:))
% loglog(F3,N3,'Color',c(3,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
xlim([0,max(F0)])
% %ylim([-75,-30])

% G1 = 20.*log10(N0./N1);
% G2 = 20.*log10(N0./N2);
% G3 = 20.*log10(N0./N3);
% G4 = 20.*log10(N0./N4);

% figure()
% % semilogx(F1,G1,'Color',c(1,:))
% semilogx(F2,G2,':','Color',c(1,:))
% hold on
% semilogx(F3,G3,'Color',c(3,:))
% % semilogx(F4,G4,':','Color',c(3,:))
% 
% 
% grid on
% xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
% ylabel({'Gain (dB)'},'Interpreter','latex')
% xlim([0,max(F0)])