% close all
% clear all
c=linspecer(9);

%% data load
fil1 = load('PMCFB01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PMCFB02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('PMCFB03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PMCFB04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F1=[f4;f3;f2;f1];
M1=[m4;m3;m2;m1];
% N1=(M1./1.44).*sqrt(2);



%% data load


figure()
loglog(F1,M1,'Color',c(3,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Noise $(\textrm{Vrms}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('PMC Feedback Point')
% legend('Laser Noise (9.5mW)','Laser Noise (4.3mW)','Laser Noise (2.2mW)','PD Dark Noise','Analyser Noise')
xlim([0,max(F1)])
% %ylim([-75,-30])

