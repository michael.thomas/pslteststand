close all
clear all
c=linspecer(9);
%% ISS PD array data load four different frequency spans put together
fil1 = load('PDA01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PDA02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('PDA03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PDA04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);
F3=[f4;f3;f2;f1];
M3=[m4;m3;m2;m1];
N3=(M3./11.23);%Converting into RIN 11.23V on PD

fil1 = load('PDB01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('PDB02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('PDB03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('PDB04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);
F4=[f4;f3;f2;f1];
M4=[m4;m3;m2;m1];
N4=(M4./10.73);%Converting into RIN 10.73V on PD

%% PMC REFL PD data load

fil0 = load('PMCREFLD.asc', '-ascii');
f0 = fil0(:,1);
m0 = fil0(:,4);
fil1 = load('PMCREFL1.asc', '-ascii');
f1 = fil1(:,1);
m1 = fil1(:,4);
fil2 = load('PMCREFL2.asc', '-ascii'); 
f2 = fil2(28:801,1);
m2 = fil2(28:801,4);
fil3 = load('PMCREFL3.asc', '-ascii'); 
f3 = fil3(28:801,1);
m3 = fil3(28:801,4);
fil4 = load('PMCREFL4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
fil5 = load('PMCREFL5.asc', '-ascii'); 
f5 = fil5(:,1);
m5 = fil5(:,4);

F1=[f5;f2];
M1=[m5;m2];
N1=(M1./0.25);%Converting into RIN
F2=[f4;f3];
M2=[m4;m3];
N2=(M2./0.25);%Converting into RIN


figure()
loglog(F1,M1,'Color',c(2,:))
hold on
loglog(F2,M2,'Color',c(3,:))
loglog(F3,M3,'Color',c(6,:))
loglog(F4,M4,':','Color',c(5,:))


grid on
legend('PMC Refl (fans on)','PMC Refl (fans off)','ISS PDA','ISS PDB')
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Laser Intensity Noise $(\textrm{Vrms}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')

xlim([0,max(f1)])
% %ylim([-75,-30])

figure()
loglog(F1,N1,'Color',c(2,:))
hold on
loglog(F2,N2,'Color',c(3,:))
loglog(F3,N3,'Color',c(6,:))
loglog(F4,N4,':','Color',c(5,:))


grid on
legend('Fans on','Fans off')
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('PMC Refl (fans on)','PMC Refl (fans off)','ISS PDA','ISS PDB')

xlim([0,max(f1)])
% %ylim([-75,-30])
