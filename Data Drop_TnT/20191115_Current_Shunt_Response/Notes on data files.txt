2019-11-15 Current_Shunt_Response
tNt LOG BOOK LINK: 11699 https://alog.ligo-la.caltech.edu/TNT/index.php?callRep=11699

Measuring the response of second stage amplifiers current shunt intensity modulation.

CSTF01.78D	Offset 400mV
CS300.78D	Offset 300mV
CS500.78D	Offset 500mV
CS401.78D	Offset 400mV lower frequency sweep.