close all 
clear all
cc=linspecer(7);
chi2 = @(mod,target) sum(((mod-target).^2));

%% Load Current Shunt TF data

fil2 = load('CSTF01.asc', '-ascii'); 
f2b = fil2(:,1);
g2b = fil2(:,4);
p2b = fil2(:,5);

fil2 = load('CS401.asc', '-ascii'); 
f2a = fil2(:,1);
g2a = fil2(:,4);
p2a = fil2(:,5);

f2 = [f2a;f2b];
g2 = [g2a;g2b];
p2 = [p2a;p2b];

fil2 = load('CS500.asc', '-ascii'); 
f3 = fil2(:,1);
g3 = fil2(:,4);
p3 = fil2(:,5);


fil2 = load('CS300.asc', '-ascii'); 
f4 = fil2(:,1);
g4 = fil2(:,4);
p4 = fil2(:,5);

%% Plot results


figure()
subplot(211)
semilogx(f4,g4,'Color',cc(4,:))
hold on
semilogx(f2,g2,'Color',cc(7,:))
semilogx(f3,g3,'Color',cc(3,:))

legend('-300mV offset','-400mV offset','-500mV offset')
grid on
ylim([-50,10])
xlim([10,1e5])
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')

subplot(212)
semilogx(f4,p4,'Color',cc(4,:))
hold on
semilogx(f2,p2,'Color',cc(7,:))
semilogx(f3,p3,'Color',cc(3,:))

grid on
ylim([-180,180])
xlim([10,1e5])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
