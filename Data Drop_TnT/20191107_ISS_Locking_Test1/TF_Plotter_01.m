close all
clear all

MMM1 = load('ISSM101.asc', '-ascii');            
MMM2 = load('ISSM201.asc', '-ascii');
freq = MMM1(:,1);     
MM1 = interp1(MMM1(:,1),MMM1(:,2:5), MMM2(:,1));
MMM1 = [MMM2(:,1), MM1];
[MMCF4,MMOF4]= obtain_TF(MMM1, MMM2, 'TnT ISS ');
m1=20.*log10(abs(MMOF4));
p1=angle(MMOF4).*(180./pi);

[a0,b0,c0,d0] = integrator(4.823e3,0.7);
sys0=ss(a0,b0,c0,d0);
[a00,b00,c00,d00] = integrator(4.421e3,0.7);
sys00=ss(a00,b00,c00,d00);
sys=series(sys0,sys00);
[gfil2,pfil2] = bode(sys,freq);

%% Plot
figure()
subplot(211)
semilogx(freq,m1,'r','LineWidth',3)
hold on
semilogx(freq,20.*log10(gfil2(:)),'k:')

grid on
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
legend('Measured OLTF','Model 2*Integrators f=4.3kHz')
xlim([min(freq),max(freq)])
ylim([-10,110])

subplot(212)
semilogx(freq,p1,'r','LineWidth',3)
hold on
semilogx(freq,pfil2(:),'k:')

grid on
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')
ylim([-180,180])
xlim([min(freq),max(freq)])