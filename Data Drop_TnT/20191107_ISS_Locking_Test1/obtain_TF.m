function [CLTF, OLTF, fig_hcl, fig_hol,freq] = obtain_TF(M1, M2, title_for_plots)

% Scrip to obtain the close loop and open loop transfer functions from the measurements of M1 and M2, obtained with the
% Stanford Signal Analyzer model SR785, as by the instructions in document 'trans_func.pdf' (which can be found in the
% TNE Logbook). 
% 
% Input arguments:
%   M1 = Sweep-sine response recorded with the system locked. Notice that it should have been converted previously from
%   extension .78D to ASCII format with the executable 'srt785.exe' available in the TNE_logbook (using the comand line
%       >> srt785 /Oasc /D /Cx,r,i M1.78D M1.asc) 
%   Notice that M1 should have been loaded to Matlab Workspace using:
%       >> load M1.ASC -ASCII
%
%   M2 = Sweep-sine response recorded with the system unlocked. Notice that it should have been converted previously from
%   extension .78D to ASCII format with the executable 'srt785.exe' available in the TNE_logbook (using the comand line
%       >> srt785 /Oasc /D /Cx,r,i M2.78D M2.asc) 
%   Notice that M2 should have been loaded to Matlab Workspace using:
%       >> load M2.ASC -ASCII
%   
%   title_for_plots = String with the common title text between the close loop and open loop transfer functions Bode
%   diagrams.
%
% Output arguments:
%   CLTF = It is the close loop transfer function of the system, given as [1 - (M1/M2)].
%
%   OLTF = It is the open loop transfer function of the system, given as [(M2/M1) - 1].
%
%   fig_hcl = Figure handle of the plot for the close loop transfer function. It is a subplot of magnitude (top) and
%   phase (bottom).
%
%   fig_hol = Figure handle of the plot for the open loop transfer function. It is a subplot of magnitude (top) and
%   phase (bottom).
%
% B. Sorazu 2009

freq = M1(:,1);     % Getting the frequency vector.
M1z = M1(:,2) + i*M1(:,3);  % Creating the complex sweep-sine responses.
M2z = M2(:,2) + i*M2(:,3);

CLTF = 1 - (M1z./M2z);

CLTF_gain = abs(CLTF);
CLTF_gain_dB = 20*log10(CLTF_gain);
CLTF_phase = 180*angle(CLTF)/pi;    % Phase converted to degrees.

OLTF = (M2z./M1z) - 1;

OLTF_gain = abs(OLTF);
OLTF_gain_dB = 20*log10(OLTF);
OLTF_gain_dB = 20*log10(OLTF_gain);
OLTF_phase = 180*angle(OLTF)/pi;    % Phase converted to degrees.

% Plotting the TFs

%Closed loop TF
fig_hcl = figure;
subplot(2,1,1);
semilogx(freq, CLTF_gain_dB,'k');
title_str = [title_for_plots ': Closed Loop TF'];
title(title_str);
ylabel('Magnitude (dB)')
grid on
xlim([min(freq),max(freq)])
subplot(2,1,2);
semilogx(freq, CLTF_phase,'k');
ylabel('Phase (degrees)')
xlabel('Frequency (Hz)')
grid on
xlim([min(freq),max(freq)])
file_title_str = [title_for_plots '_CLTF.fig'];
file_title_str_png = [title_for_plots '_CLTF.png'];
savefig(file_title_str)
saveas(gcf,file_title_str_png)
%Open loop TF
fig_hol = figure;
subplot(2,1,1);
semilogx(freq, OLTF_gain_dB,'k');
title_str = [title_for_plots ': Open Loop TF'];
title(title_str);
ylabel('Magnitude (dB)')
grid on
xlim([min(freq),max(freq)])
subplot(2,1,2);
semilogx(freq, OLTF_phase,'k');
ylabel('Phase (degrees)')
xlabel('Frequency (Hz)')
grid on
xlim([min(freq),max(freq)])
file_title_str = [title_for_plots '_OLTF.fig'];
file_title_str_png = [title_for_plots '_OLTF.png'];
savefig(file_title_str)
saveas(gcf,file_title_str_png)

%close all