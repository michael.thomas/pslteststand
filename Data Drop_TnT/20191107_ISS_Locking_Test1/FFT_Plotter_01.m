close all
clear all
c=linspecer(9);
%% Load TF data
MMM1 = load('ISSM101.asc', '-ascii');            
MMM2 = load('ISSM201.asc', '-ascii');
freq = MMM1(:,1);     
MM1 = interp1(MMM1(:,1),MMM1(:,2:5), MMM2(:,1));
MMM1 = [MMM2(:,1), MM1];
[MMCF4,MMOF4]= obtain_TF(MMM1, MMM2, 'TnT ISS ');

OLG1=20.*log10(abs(MMOF4));
OLP1=angle(MMOF4).*(180./pi);
CLG1=20.*log10(abs(MMCF4));
CLP1=angle(MMCF4).*(180./pi);
%% data load
fil1 = load('ISSINL01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('ISSINL02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('ISSINL03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('ISSINL04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F1=[f4;f3;f2;f1];
M1=[m4;m3;m2;m1];
N1=(M1./1.44).*sqrt(2);

fil1 = load('ISSOOL1.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('ISSOOL2.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('ISSOOL3.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('ISSOOL4.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);
F2=[f4;f3;f2;f1];
M2=[m4;m3;m2;m1];
N2=(M2./1.51).*sqrt(2);

%% data load

fil1 = load('ISSUNS01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('ISSUNS02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('ISSUNS03.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,4);
fil4 = load('ISSUNS04.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,4);

F0=[f4;f3;f2;f1];
M0=[m4;m3;m2;m1];
N0=(M1./1.51).*sqrt(2);


figure()
loglog(F1,M1,'Color',c(2,:))
hold on
loglog(F2,M2,'Color',c(3,:))
loglog(F0,M0,'Color',c(6,:))

grid on
legend('In-loop stabilised noise','Out of loop stabilised noise','Unstabilised noise')
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')

% legend('Laser Noise (9.5mW)','Laser Noise (4.3mW)','Laser Noise (2.2mW)','PD Dark Noise','Analyser Noise')
xlim([0,max(f1)])
% %ylim([-75,-30])

%%
MM1 = interp1(F1,M1,freq);
adj1 = (MMOF4./MMCF4);
aG1=20.*log10(abs(adj1));
aP1=angle(adj1).*(180./pi);

Gainest1 = 20.*log10(M0./M1);
Gainest2 = 20.*log10(M0./M2);

figure()
semilogx(F1,Gainest1,'Color',c(7,:))
hold on
semilogx(F1,Gainest2,'Color',c(9,:))
semilogx(freq,aG1,':','Color',c(8,:))
legend('In-loop estimate','Out-of-loop estimate','Measured TF estimate')
grid on
xlim([0,max(F1)])
% ylim([-10,100])
xlabel('Frequency (Hz)')
ylabel('Gain')
