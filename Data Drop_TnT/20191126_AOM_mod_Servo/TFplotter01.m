close all 
clear all
cc=linspecer(9);
chi2 = @(mod,target) sum(((mod-target).^2));
%% Load CMod TF data AMPII

fil1 = load('15-TF1.asc', '-ascii'); 
f11 = fil1(:,1);
g11 = fil1(:,4);
p11 = fil1(:,5);
OLTF11 = MP2complex(10.^(g11./20),p11);
CLTF11 = 1-1./(OLTF11+1);
CLG11 = 20.*log10(abs(CLTF11));
CLP11 = angle(CLTF11).*(180./pi);

fil2 = load('20-TF1.asc', '-ascii'); 
f12 = fil2(:,1);
g12 = fil2(:,4);
p12 = fil2(:,5);
OLTF12 = MP2complex(10.^(g12./20),p12);
CLTF12 = 1-1./(OLTF12+1);
CLG12 = 20.*log10(abs(CLTF12));
CLP12 = angle(CLTF12).*(180./pi);

fil2 = load('25-TF1.asc', '-ascii'); 
f13 = fil2(:,1);
g13 = fil2(:,4);
p13 = fil2(:,5);
OLTF13 = MP2complex(10.^(g13./20),p13);
CLTF13 = 1-1./(OLTF13+1);
CLG13 = 20.*log10(abs(CLTF13));
CLP13 = angle(CLTF13).*(180./pi);

UGP1 = f11(find(g11<0,1));
UGP2 = f12(find(g12<0,1));
UGP3 = f13(find(g13<0,1));
PM1 = p11(find(g11<0,1));
PM2 = p12(find(g12<0,1));
PM3 = p13(find(g13<0,1));

%%
figure()
subplot(211)
semilogx(f11,g11,'Color',cc(1,:))
hold on
semilogx(f12,g12,'Color',cc(2,:))
semilogx(f13,g13,'Color',cc(3,:))
semilogx([min(f11),max(f11)],[0,0],':','Color',[cc(1,:),0.5])
semilogx([UGP1,UGP1],[-10,60],':','Color',[cc(1,:),0.5])
semilogx([UGP2,UGP2],[-10,60],':','Color',[cc(2,:),0.5])
semilogx([UGP3,UGP3],[-10,60],':','Color',[cc(3,:),0.5])


grid on
xlim([min(f11),max(f11)])
ylim([-10,60])
title('Open Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('15dB','20dB','25dB');
title(leg,'Gain:')

subplot(212)
semilogx(f11,p11,'Color',cc(1,:))
hold on
semilogx(f12,p12,'Color',cc(2,:))
semilogx(f13,p13,'Color',cc(3,:))

semilogx([UGP1,UGP1],[-180,180],':','Color',[cc(1,:),0.5])
semilogx([UGP2,UGP2],[-180,180],':','Color',[cc(2,:),0.5])
semilogx([UGP3,UGP3],[-180,180],':','Color',[cc(3,:),0.5])

semilogx([min(f11),max(f11)],[PM1,PM1],':','Color',[cc(1,:),0.5])
semilogx([min(f11),max(f11)],[PM2,PM2],':','Color',[cc(2,:),0.5])
semilogx([min(f11),max(f11)],[PM3,PM3],':','Color',[cc(3,:),0.5])


grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')

figure()
subplot(211)
semilogx(f11,CLG11,'Color',cc(1,:))
hold on
semilogx(f12,CLG12,'Color',cc(2,:))
semilogx(f13,CLG13,'Color',cc(3,:))


grid on
xlim([min(f11),max(f11)])
title('Closed Loop TF')
xlabel('Frequency (Hz)')
ylabel('Gain (dB)')
leg = legend('15dB','20dB','25dB');
title(leg,'Gain')

subplot(212)
semilogx(f11,CLP11,'Color',cc(1,:))
hold on
semilogx(f12,CLP12,'Color',cc(2,:))
semilogx(f13,CLP13,'Color',cc(3,:))

grid on
ylim([-180,180])
xlim([min(f11),max(f11)])
xlabel('Frequency (Hz)')
ylabel('Phase (deg)')