% close all
% clear all
c=linspecer(9);
%%
LISOPD = importdata('ISS_PDFILT.out');
fPD = LISOPD.data(:,1);
gPD = 10.^(LISOPD.data(:,2)./20);
pPD = LISOPD.data(:,3);
PDTF0 = MP2complex(gPD,pPD);
%% data load

fil1 = load('25-NN1A.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('25-NN2A.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('25-NN3A.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('25-NN4A.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F0=[f4;f3;f2;f1];
n25=([m4;m3;m2;m1])./15;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
gPD = interp1(fPD,gPD,F0);
N25=n25./gPD;

fil1 = load('25-NN1B.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('25-NN2B.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('25-NN3B.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('25-NN4B.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F1=[f4;f3;f2;f1];
n25b=([m4;m3;m2;m1])./15;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fil1 = load('20-NN1A.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('20-NN2A.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('20-NN3A.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('20-NN4A.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F0=[f4;f3;f2;f1];
n20=([m4;m3;m2;m1])./15;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
N20=n20./gPD;

fil1 = load('20-NN1B.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('20-NN2B.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('20-NN3B.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('20-NN4B.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F1=[f4;f3;f2;f1];
n20b=([m4;m3;m2;m1])./15;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fil1 = load('15-NN1A.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('15-NN2A.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('15-NN3A.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('15-NN4A.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
F0=[f4;f3;f2;f1];
n15=([m4;m3;m2;m1])./15;
% nO30=([mO4;mO3;mO2;mO1])./11.25;
N15=n15./gPD;

fil1 = load('15-NN1B.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,2);
fil2 = load('15-NN2B.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,2);
fil3 = load('15-NN3B.asc', '-ascii'); 
f3 = fil3(102:801,1);
m3 = fil3(102:801,2);
fil4 = load('15-NN4B.asc', '-ascii'); 
f4 = fil4(:,1);
m4 = fil4(:,2);
n15b=([m4;m3;m2;m1])./15;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fil1 = load('15-TF1.asc', '-ascii'); 
f11 = fil1(:,1);
g11 = fil1(:,4);
p11 = fil1(:,5);
OLTF11 = MP2complex(10.^(g11./20),p11);
CLTF11 = 1-1./(OLTF11+1);
CLG11 = 20.*log10(abs(CLTF11));
CLP11 = angle(CLTF11).*(180./pi);

fil2 = load('20-TF1.asc', '-ascii'); 
f12 = fil2(:,1);
g12 = fil2(:,4);
p12 = fil2(:,5);
OLTF12 = MP2complex(10.^(g12./20),p12);
CLTF12 = 1-1./(OLTF12+1);
CLG12 = 20.*log10(abs(CLTF12));
CLP12 = angle(CLTF12).*(180./pi);

fil2 = load('25-TF1.asc', '-ascii'); 
f13 = fil2(:,1);
g13 = fil2(:,4);
p13 = fil2(:,5);
OLTF13 = MP2complex(10.^(g13./20),p13);
CLTF13 = 1-1./(OLTF13+1);
CLG13 = 20.*log10(abs(CLTF13));
CLP13 = angle(CLTF13).*(180./pi);

%%
NN25 = interp1(F0,N25,f13);
NN20 = interp1(F0,N20,f12);
NN15 = interp1(F0,N15,f11);

FRN1 = abs((NN25./CLTF13).*OLTF13);
FRN2 = abs((NN20./CLTF12).*OLTF12);
FRN3 = abs((NN15./CLTF11).*OLTF11);

%%
figure()
loglog(F0,N25,'Color',c(2,:))
hold on
loglog(F0,n25b,'--','Color',c(2,:))
loglog(F0,N20,'Color',c(3,:))
loglog(F0,n20b,'--','Color',c(3,:))
loglog(F0,N15,'Color',c(4,:))
loglog(F0,n15b,'--','Color',c(4,:))

loglog(f11,FRN1,':','Color',c(2,:))
loglog(f12,FRN2,':','Color',c(3,:))
loglog(f13,FRN3,':','Color',c(4,:))

grid on
legend('25dB','20dB','15dB');
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
xlim([0,max(F0)])
% %ylim([-75,-30])

