function[C]=MP2complex(m,p)
g=m(:);
ph=p(:);
R = g.*cosd(ph);
I = g.*sind(ph);
C = R + I.*1i;
end

