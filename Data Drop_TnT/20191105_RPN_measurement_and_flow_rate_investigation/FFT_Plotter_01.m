close all
clear all
c=linspecer(9);
%% Watchdog after amp II data load

fil0 = load('WD1CARK.asc', '-ascii');
f0 = fil0(:,1);
m0 = fil0(:,4);
fil1 = load('WD1FFT01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('WD1FFT02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('WD1FFT03.asc', '-ascii'); 
f3 = fil3(202:801,1);
m3 = fil3(202:801,4);
fil4 = load('WD1FFT04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);

fil5 = load('WD1FFT05.asc', '-ascii'); 
f5 = fil5(102:801,1);
m5 = fil5(102:801,4);
fil6 = load('WD1FFT06.asc', '-ascii'); 
f6 = fil6(102:801,1);
m6 = fil6(102:801,4);
fil7 = load('WD1FFT07.asc', '-ascii'); 
f7 = fil7(202:801,1);
m7 = fil7(202:801,4);
fil8 = load('WD1FFT08.asc', '-ascii'); 
f8 = fil8(5:801,1);
m8 = fil8(5:801,4);

F0 = [f4;f3;f2;f1];
M0 = [m4;m3;m2;m1];

F1 = [f8;f7;f6;f5];
M1 = [m8;m7;m6;m5];
N1 = M1./10.4;

figure()
loglog(F1,M0./8.8,'Color',c(2,:))
hold on
loglog(F0,M1./10.4,'Color',c(1,:))
% loglog(f0,m0,'k:')

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
title('Laser intensity noise on Watchdog PD after first stage amplifier')
ylabel({'\textbf{Laser Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('Without focussing lens','with f=100mm lens')

% legend('Laser Noise (9.5mW)','Laser Noise (4.3mW)','Laser Noise (2.2mW)','PD Dark Noise','Analyser Noise')
xlim([0,max(F1)])
% %ylim([-75,-30])


%% Watchdog after amp I data load
fil1 = load('WD2FFT01.asc', '-ascii');
f1 = fil1(102:801,1);
m1 = fil1(102:801,4);
fil2 = load('WD2FFT02.asc', '-ascii'); 
f2 = fil2(102:801,1);
m2 = fil2(102:801,4);
fil3 = load('WD2FFT03.asc', '-ascii'); 
f3 = fil3(202:801,1);
m3 = fil3(202:801,4);
fil4 = load('WD2FFT04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);

fil5 = load('WD2FFT30.asc', '-ascii'); 
f5 = fil5(102:801,1);
m5 = fil5(102:801,4);
fil6 = load('WD2FFT31.asc', '-ascii'); 
f6 = fil6(102:801,1);
m6 = fil6(102:801,4);
fil7 = load('WD2FFT32.asc', '-ascii'); 
f7 = fil7(202:801,1);
m7 = fil7(202:801,4);
fil8 = load('WD2FFT33.asc', '-ascii'); 
f8 = fil8(5:801,1);
m8 = fil8(5:801,4);

fil5 = load('WD2FFT20.asc', '-ascii'); 
fa1 = fil5(:,1);
ma1 = fil5(:,4);
fil6 = load('WD2FFT21.asc', '-ascii'); 
fa2 = fil6(:,1);
ma2 = fil6(:,4);
fil7 = load('WD2FFT22.asc', '-ascii'); 
fa3 = fil7(:,1);
ma3 = fil7(:,4);
fil8 = load('WD2FFT23.asc', '-ascii'); 
fa4 = fil8(:,1);
ma4 = fil8(:,4);
fil8 = load('WD2FFT24.asc', '-ascii'); 
fa5 = fil8(:,1);
ma5 = fil8(:,4);

F2 = [f4;f3;f2;f1];
M2 = [m4;m3;m2;m1];
N2 = M2./2.45;

F3 = [f8;f7;f6;f5];
M3 = [m8;m7;m6;m5];
N3 = M3./2;

figure()
loglog(F1,N1,'Color',c(1,:))
hold on
loglog(F2,N2,'Color',c(3,:))
loglog(F3,N3,'Color',c(4,:))
% loglog(f0,m0./2.5,'k:')

% loglog(F1,M1,':','Color',c(1,:))
% loglog(F1,M2,':','Color',c(3,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
title('Laser Intensity Noise on Watchdog PD')
ylabel({'\textbf{Relative Intensity Noise $(\textrm{1}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
legend('after amp II (flow rate=2l/min)','after amp I (flow rate=2l/min)','after amp I (flow rate=1l/min)')

figure()
loglog(fa1,ma1,'Color',c(3,:))
hold on
loglog(fa2,ma2,'Color',c(5,:))
loglog(fa3,ma3,'Color',c(6,:))
loglog(fa4,ma4,':','Color',c(7,:))
loglog(fa5,ma5,':','Color',c(8,:))
xlim([0,max(fa1)])
% loglog(f0,m0,'k:')

% %ylim([-75,-30])
grid on
legend('flow rate 2l/min','flow rate 2l/min','flow rate 2l/min','flow rate 1.2l/min','flow rate 1l/min')
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
title('Wandering Laser Intensity Noise on Watchdog PD after Amp I')
ylabel({'\textbf{Intensity Noise $(\textrm{Vrms}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')

%% ISS PDA data load
fil1 = load('PDAFFT01.asc', '-ascii');
f1 = fil1(1:801,1);
m1 = fil1(1:801,4);
fil2 = load('PDAFFT02.asc', '-ascii'); 
f2 = fil2(1:801,1);
m2 = fil2(1:801,4);
fil3 = load('PDAFFT03.asc', '-ascii'); 
f3 = fil3(2:801,1);
m3 = fil3(2:801,4);
fil4 = load('PDAFFT04.asc', '-ascii'); 
f4 = fil4(5:801,1);
m4 = fil4(5:801,4);

F4 = [f4;f3;f2;f1];
M4 = [m4;m3;m2;m1];
N4 = m1./104;

figure()
% hold on
loglog(f1,m1,'Color',c(6,:))
hold on
loglog(f2,m2,'Color',c(7,:))
loglog(f3,m3,'Color',c(8,:))
loglog(f4,m4,'Color',c(9,:))

grid on
xlabel({'\textbf{Frequency (Hz)}'},'Interpreter','latex')
title('Laser Intensity Noise on ISS PDA')
ylabel({'\textbf{Intensity Noise $(\textrm{Vrms}/\sqrt{\textrm{Hz}})$}'},'Interpreter','latex')
