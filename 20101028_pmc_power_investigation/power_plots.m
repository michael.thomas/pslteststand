%% load in data 
clear

%choose gps seconds for end time, converter here https://www.andrews.edu/~tzs/timeconv/timeconvert.php
end_time = 1257520308-60*60;
duration = 2*60*60*24;
start_time = end_time - duration;

chans = {'X2:PSL-PWR_PMC_TRANS_OUTPUT',...
    'X2:PSL-PWR_PMC_REFL_OUTPUT',...
    'X2:PSL-PWR_PMC_SUM',...
    'X2:PSL-PMC_TRIGGER_OUTPUT',...
    'X2:PSL-PMC_HEATER_CALI_INMON'...
    'X2:PSL-PMC_TEMP_OUTPUT',...
    'X2:PSL-PMC_HV_MON_AVG',...
    'X2:PSL-PMC_GAIN',...
    'X2:PSL-PMC_TEMP_CTRL_SIGNAL',...
    'X2:PSL-OSC_BASEPL_CENTTEMP',...
    'X2:PSL-OSC_BOXHUM',...
    'X2:PSL-ISS_AOM_DRIVER_MON_OUTPUT'};
min_trend_data =  get_data(chans,'minute',start_time,2*24*60*60);
%sec_trend_data =  get_data(chans,'sec',start_time,2*24*60*60);

%% make variables time series  

time = 0:1:length(min_trend_data(1).data)-1; %time series in steps of the data rate (1 sample/min)
time_hour = time./60; %hours
PMC_trans = min_trend_data(1).data; %Watts
PMC_ref = min_trend_data(2).data; %Watts
PMC_sum = min_trend_data(3).data; %Watts
PMC_trigger = min_trend_data(4).data; %Volts
PMC_heater = min_trend_data(5).data; %Volts?
PMC_temp = min_trend_data(6).data; %Kelvin?
PMC_HV_mon = min_trend_data(7).data; %Volts (1:50 of actual voltage)
PMC_gain = min_trend_data(8).data; %gain of PMC loop, no units
PMC_temp_control_signal = min_trend_data(9).data; %not sure on units
TNT_temp = min_trend_data(10).data; %deg C
TNT_humidity = min_trend_data(11).data; %water/m^3?
AOM = min_trend_data(12).data;
%% Plot time series

figure(1)
subplot(3,3,1)
plot(time_hour,PMC_trans)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC transmitted power')
axis tight
grid minor

subplot(3,3,2)
plot(time_hour,PMC_ref)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC reflected power')
axis tight
grid minor

subplot(3,3,3)
plot(time_hour,PMC_sum)
xlabel('Time (hour)')
ylabel('Power (W)')
title('PMC sum power')
axis tight
grid minor

subplot(3,3,4)
plot(time_hour,PMC_trigger)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('PMC trigger signal')
axis tight
grid minor

subplot(3,3,5)
plot(time_hour,AOM)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('AOM signal')
axis tight
grid minor

subplot(3,3,6)
plot(time_hour,PMC_temp)
xlabel('Time (hour)')
ylabel('Temperature (K)')
title('PMC Temperature')
axis tight
grid minor

subplot(3,3,7)
plot(time_hour,PMC_heater)
xlabel('Time (hour)')
ylabel('units?')
title('PMC heater signal')
axis tight
grid minor

subplot(3,3,8)
plot(time_hour,PMC_HV_mon)
xlabel('Time (hour)')
ylabel('Voltage (V)')
title('PMC HV mon (1:50)')
axis tight
grid minor

subplot(3,3,9)
plot(time_hour,TNT_humidity)
xlabel('Time (hour)')
ylabel('Humidity (water vaper/m^3)')
title('TNT lab Humidity')
axis tight
grid minor

set(gcf,'Position',[0 0 900 900])
figure_1_name = ['PMC_state_',num2str(start_time),'to_',num2str(end_time)]
print(figure_1_name,'-dpng')

%% pwelch set up

%set up measurement
fs = 1/60; %sample rate is 1 per min, in Hz this is 1/60.
f_nyquist = fs/2;
%choose this number (Hz)
f_resolution = 2e-5;
len = floor(fs/f_resolution);
f_resolution = fs/len;

%make round to number of the form 2^a 3^b 5^c 7^d 11^e 13^f
% Make the ASD by following this guide: https://holometer.fnal.gov/GH_FFT.pdf
% window based on HFT116D
% need 78.2% overlap

j = linspace(1,len,len);
z = 2*pi.*j./len;
window_fn = 0.5.*(1-cos(z)); %Hann window

%calculation of effective noise bandwidth, sometimes this is useful
s1 = sum(window_fn);
s2 = sum(window_fn.*window_fn);
ENBW = fs*s2/(s1^2);
NENBW = len*s2/(s1^2);
overlap = 0.5;
disp('...')
disp(['nyquist frequency: ',num2str(f_nyquist),' Hz'])
disp(['frequency Resolution: ',num2str(f_resolution),' Hz'])
disp(['Number of frequency Bins: ',num2str(len), ' of width ',...
num2str(f_resolution), ' Hz'])
disp(['Check for speediness of FFTW: ',num2str(factor(len))])
disp(['Effective Noise Bandwidth: ',num2str(ENBW), 'Hz'])
disp(['Window overlap: ',num2str(100*overlap), '%']);
disp(['Normalised Effective Noise Bandwidth: ',num2str(NENBW), ' bins']);

%% pwelch calculation

TNT_humidity_DC_remove = TNT_humidity-mean(TNT_humidity);
PMC_heater_DC_remove = PMC_heater - mean(PMC_heater);
PMC_trans_DC_remove = PMC_trans - mean(PMC_trans);
AOM_DC_remove = AOM - mean(AOM);

[TNT_humidity_PSD,f] = pwelch(TNT_humidity_DC_remove,window_fn,floor(overlap*len),len,fs);
PMC_heater_PSD = pwelch(PMC_heater_DC_remove,window_fn,floor(overlap*len),len,fs);
PMC_trans_PSD = pwelch(PMC_trans_DC_remove,window_fn,floor(overlap*len),len,fs);
AOM_PSD = pwelch(AOM_DC_remove,window_fn,floor(overlap*len),len,fs);

TNT_humidity_ASD = sqrt(TNT_humidity_PSD);
PMC_heater_ASD = sqrt(PMC_heater_PSD);
PMC_trans_ASD = sqrt(PMC_trans_PSD);
AOM_ASD = sqrt(AOM_PSD);

figure(2)
subplot(2,2,1)
loglog(f,TNT_humidity_ASD)
xlabel('Frequency (Hz)')
ylabel('humidity units/\surd(Hz)')
title('Lab temperature')
grid minor
axis tight

subplot(2,2,2)
loglog(f,PMC_trans_ASD)
xlabel('Frequency (Hz)')
ylabel('W/\surd(Hz)')
title('PMC Transmited Power')
grid minor
axis tight

subplot(2,2,3)
loglog(f,PMC_heater_ASD)
xlabel('Frequency (Hz)')
ylabel('units?/\surd(Hz)')
title('Heater signal')
grid minor
axis tight

subplot(2,2,4)
loglog(f,AOM_ASD)
xlabel('Frequency (Hz)')
ylabel('V/\surd(Hz)')
title('AOM signal')
grid minor
axis tight

figure_2_name = ['PMC_state_ASD_',num2str(start_time),'to_',num2str(end_time)]
print(figure_2_name,'-dpng')